﻿using eastLIFEnet.World;
using SampSharp.GameMode.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eastLIFEnet.Controllers
{
    class VehicleController : BaseVehicleController
    {
        public override void RegisterTypes()
        {
            base.RegisterTypes();
            Vehicle.Register<Vehicle>();
        }
    }
}
