﻿using SampSharp.GameMode.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SampSharp.GameMode;
using eastLIFEnet.Commands;
using eastLIFEnet.Modules.Server.Commands;

namespace eastLIFEnet.Controllers
{
    class ECommandController : CommandController
    {
        public static ECommandManager commandManager = null;

        public override void RegisterServices(BaseMode gameMode, GameModeServiceContainer serviceContainer)
        {
            commandManager = new ECommandManager(gameMode);
            //serviceContainer.AddService(CommandsManager);
            CommandsManager = commandManager;

            commandManager.RegisterCommands(gameMode.GetType());
            //base.RegisterServices(gameMode, serviceContainer);
        }

        public override void RegisterEvents(BaseMode gameMode)
        {

        }
    }
}
