﻿using eastLIFEnet.World;
using SampSharp.GameMode;
using SampSharp.GameMode.Definitions;
using SampSharp.GameMode.Display;
using SampSharp.GameMode.Events;

namespace eastLIFEnet
{
    class InventoryTextDraw
    {
        private bool m_IsShowing;
        private Player m_Player;

        PlayerTextDraw mainBackground;
        PlayerTextDraw inventoryHeading;
        PlayerTextDraw tableHeadingBackground;
        PlayerTextDraw tableHeadingNumber;
        PlayerTextDraw tableHeadingItemName;
        PlayerTextDraw tableHeadingItemQuantity;
        PlayerTextDraw tableHeadingItemWeight;
        PlayerTextDraw mainInventoryBackground;
        PlayerTextDraw[] itemNumber = new PlayerTextDraw[Inventory.MAX_SLOTS];
        PlayerTextDraw[] itemName = new PlayerTextDraw[Inventory.MAX_SLOTS];
        PlayerTextDraw[] itemQuantity = new PlayerTextDraw[Inventory.MAX_SLOTS];
        PlayerTextDraw[] itemWeight = new PlayerTextDraw[Inventory.MAX_SLOTS];
        PlayerTextDraw[] itemWeightUnit = new PlayerTextDraw[Inventory.MAX_SLOTS];
        PlayerTextDraw closeButton;

        public bool IsShowing
        {
            get
            {
                return m_IsShowing;
            }
        }

        public InventoryTextDraw(Player player)
        {
            m_Player = player;
        }

        public void Show()
        {
            mainBackground.Show();
            inventoryHeading.Show();
            tableHeadingBackground.Show();
            tableHeadingNumber.Show();
            tableHeadingItemName.Show();
            tableHeadingItemQuantity.Show();
            tableHeadingItemWeight.Show();
            mainInventoryBackground.Show();
            closeButton.Show();
            for(int i = 0; i < 2; i++)
            {
                itemNumber[i].Show();
                itemName[i].Show();
                itemQuantity[i].Show();
                itemWeight[i].Show();
                itemWeightUnit[i].Show();
            }

            m_IsShowing = true;
        }

        public void Hide()
        {
            if (!m_IsShowing) return;

            mainBackground.Hide();
            inventoryHeading.Hide();
            tableHeadingBackground.Hide();
            tableHeadingNumber.Hide();
            tableHeadingItemName.Hide();
            tableHeadingItemQuantity.Hide();
            tableHeadingItemWeight.Hide();
            mainInventoryBackground.Hide();
            closeButton.Hide();
            for (int i = 0; i < 2; i++)
            {
                itemNumber[i].Hide();
                itemName[i].Hide();
                itemQuantity[i].Hide();
                itemWeight[i].Hide();
                itemWeightUnit[i].Hide();
            }

            m_IsShowing = false;
        }

        public void CreateTextdraws()
        {
            #region MainBackground
            mainBackground = new PlayerTextDraw(m_Player, new Vector2(70.375000, 124.764999), "l");
            mainBackground.LetterSize = new Vector2(0.278124, 23.043333);
            mainBackground.Width = 436f;
            mainBackground.Height = 5.420029f;
            mainBackground.Alignment = TextDrawAlignment.Left;
            mainBackground.ForeColor = 0;
            mainBackground.UseBox = true;
            mainBackground.BoxColor = 100;
            mainBackground.Shadow = 0;
            mainBackground.Outline = 0;
            mainBackground.BackColor = 255;
            mainBackground.Font = TextDrawFont.Slim;
            mainBackground.Proportional = true;
            mainBackground.Shadow = 0;
            #endregion

            #region InventoryHeading
            inventoryHeading = new PlayerTextDraw(m_Player, new Vector2(226.250000, 125.049980), "Inventorius");
            inventoryHeading.LetterSize = new Vector2(0.213124, 0.940832);
            inventoryHeading.Alignment = TextDrawAlignment.Left;
            inventoryHeading.ForeColor = -1;
            inventoryHeading.Shadow = 0;
            inventoryHeading.Outline = 0;
            inventoryHeading.BackColor = 255;
            inventoryHeading.Font = TextDrawFont.Slim;
            inventoryHeading.Proportional = true;
            inventoryHeading.Shadow = 0;
            #endregion

            #region TableHeadingBackground
            tableHeadingBackground = new PlayerTextDraw(m_Player, new Vector2(71.250000, 138.466659), "table heading bg");
            tableHeadingBackground.LetterSize = new Vector2(0.384373, 1.197499);
            tableHeadingBackground.Width = 436f;
            tableHeadingBackground.Height = 0f;
            tableHeadingBackground.Alignment = TextDrawAlignment.Left;
            tableHeadingBackground.ForeColor = 0;
            tableHeadingBackground.UseBox = true;
            tableHeadingBackground.BoxColor = 50;
            tableHeadingBackground.Shadow = 0;
            tableHeadingBackground.Outline = 0;
            tableHeadingBackground.BackColor = 255;
            tableHeadingBackground.Font = TextDrawFont.Normal;
            tableHeadingBackground.Proportional = true;
            tableHeadingBackground.Shadow = 0;
            #endregion

            #region TableHeadingNumber
            tableHeadingNumber = new PlayerTextDraw(m_Player, new Vector2(76.875000, 139.050003), "#");
            tableHeadingNumber.LetterSize = new Vector2(0.281874, 1.004999);
            tableHeadingNumber.Alignment = TextDrawAlignment.Left;
            tableHeadingNumber.ForeColor = -1;
            tableHeadingNumber.Shadow = 0;
            tableHeadingNumber.Outline = 0;
            tableHeadingNumber.BackColor = 255;
            tableHeadingNumber.Font = TextDrawFont.Pricedown;
            tableHeadingNumber.Proportional = true;
            tableHeadingNumber.Shadow = 0;
            #endregion

            #region TableHeadingItemName
            tableHeadingItemName = new PlayerTextDraw(m_Player, new Vector2(101.250000, 139.633346), "Pavadinimas");
            tableHeadingItemName.LetterSize = new Vector2(0.244999, 0.894164);
            tableHeadingItemName.Alignment = TextDrawAlignment.Left;
            tableHeadingItemName.ForeColor = -1;
            tableHeadingItemName.Shadow = 0;
            tableHeadingItemName.Outline = 0;
            tableHeadingItemName.BackColor = 255;
            tableHeadingItemName.Font = TextDrawFont.Normal;
            tableHeadingItemName.Proportional = true;
            tableHeadingItemName.Shadow = 0;
            #endregion

            #region TableHeadingItemQuantity
            tableHeadingItemQuantity = new PlayerTextDraw(m_Player, new Vector2(278.125000, 139.633346), "Kiekis");
            tableHeadingItemQuantity.LetterSize = new Vector2(0.244999, 0.894164);
            tableHeadingItemQuantity.Alignment = TextDrawAlignment.Left;
            tableHeadingItemQuantity.ForeColor = -1;
            tableHeadingItemQuantity.Shadow = 0;
            tableHeadingItemQuantity.Outline = 0;
            tableHeadingItemQuantity.BackColor = 255;
            tableHeadingItemQuantity.Font = TextDrawFont.Normal;
            tableHeadingItemQuantity.Proportional = true;
            tableHeadingItemQuantity.Shadow = 0;
            #endregion

            #region TableHeadingItemWeight
            tableHeadingItemWeight = new PlayerTextDraw(m_Player, new Vector2(368.125000, 139.633346), "Svoris");
            tableHeadingItemWeight.LetterSize = new Vector2(0.244999, 0.894164);
            tableHeadingItemWeight.Alignment = TextDrawAlignment.Left;
            tableHeadingItemWeight.ForeColor = -1;
            tableHeadingItemWeight.Shadow = 0;
            tableHeadingItemWeight.Outline = 0;
            tableHeadingItemWeight.BackColor = 255;
            tableHeadingItemWeight.Font = TextDrawFont.Normal;
            tableHeadingItemWeight.Proportional = true;
            tableHeadingItemWeight.Shadow = 0;
            #endregion

            for (int i = 0; i < Inventory.MAX_SLOTS; i++)
            {
                #region ItemNumber
                itemNumber[i] = new PlayerTextDraw(m_Player, new Vector2(79.375000, 162.966720 + (10.700653f * i)), "1")
                {
                    LetterSize = new Vector2(0.200624, 0.993332),
                    Alignment = TextDrawAlignment.Center,
                    ForeColor = -1,
                    Shadow = 0,
                    Outline = 0,
                    BackColor = 255,
                    Font = TextDrawFont.Slim,
                    Proportional = true
                };
                #endregion

                #region ItemName
                itemName[i] = new PlayerTextDraw(m_Player, new Vector2(100.000000, 162.966720 + (10.700653f * i)), "Item pavadinimas")
                {
                    LetterSize = new Vector2(0.200624, 0.993332),
                    Alignment = TextDrawAlignment.Left,
                    ForeColor = -1,
                    Shadow = 0,
                    Outline = 0,
                    BackColor = 255,
                    Font = TextDrawFont.Slim,
                    Proportional = true,
                    Selectable = true
                };
                #endregion

                #region ItemQuantity
                itemQuantity[i] = new PlayerTextDraw(m_Player, new Vector2(288.125000, 162.966720 + (10.700653f * i)), "0")
                {
                    LetterSize = new Vector2(0.200624, 0.993332),
                    Alignment = TextDrawAlignment.Center,
                    ForeColor = -1,
                    Shadow = 0,
                    Outline = 0,
                    BackColor = 255,
                    Font = TextDrawFont.Slim,
                    Proportional = true
                };
                #endregion

                #region ItemWeight
                itemWeight[i] = new PlayerTextDraw(m_Player, new Vector2(380.291687, 162.966720 + (10.700653f * i)), "0")
                {
                    LetterSize = new Vector2(0.200624, 0.993332),
                    Alignment = TextDrawAlignment.Right,
                    ForeColor = -1,
                    Shadow = 0,
                    Outline = 0,
                    BackColor = 255,
                    Font = TextDrawFont.Slim,
                    Proportional = true
                };
                #endregion

                #region ItemWeightUnit
                itemWeightUnit[i] = new PlayerTextDraw(m_Player, new Vector2(379.925109, 162.966720 + (10.700653f * i)), "0")
                {
                    LetterSize = new Vector2(0.200624, 0.993332),
                    Alignment = TextDrawAlignment.Left,
                    ForeColor = -1,
                    Shadow = 0,
                    Outline = 0,
                    BackColor = 255,
                    Font = TextDrawFont.Slim,
                    Proportional = true
                };
                #endregion
            }

            #region MainInventoryBackground
            mainInventoryBackground = new PlayerTextDraw(m_Player, new Vector2(71.250000, 161.216674), "main inventory bgh");
            mainInventoryBackground.LetterSize = new Vector2(0.176872, 18.977500);
            mainInventoryBackground.Width = 436f;
            mainInventoryBackground.Alignment = TextDrawAlignment.Left;
            mainInventoryBackground.ForeColor = 0;
            mainInventoryBackground.UseBox = true;
            mainInventoryBackground.BoxColor = 50;
            mainInventoryBackground.Shadow = 0;
            mainInventoryBackground.Outline = 0;
            mainInventoryBackground.BackColor = 255;
            mainInventoryBackground.Font = TextDrawFont.Normal;
            mainInventoryBackground.Proportional = true;
            mainInventoryBackground.Shadow = 0;
            #endregion

            #region CloseButton
            closeButton = new PlayerTextDraw(m_Player, new Vector2(429.524932, 109.383323), "X");
            closeButton.LetterSize = new Vector2(0.453125, 0.999166);
            closeButton.Width = 0.0f;
            closeButton.Height = 13f;
            closeButton.Alignment = TextDrawAlignment.Center;
            closeButton.ForeColor = -16776961;
            closeButton.UseBox = true;
            closeButton.BoxColor = 150;
            closeButton.Shadow = 0;
            closeButton.Outline = 0;
            closeButton.BackColor = 255;
            closeButton.Font = TextDrawFont.Slim;
            closeButton.Proportional = true;
            closeButton.Shadow = 0;
            closeButton.Selectable = true;
            closeButton.Click += (object sender, ClickPlayerTextDrawEventArgs e) =>
            {
                Hide();
            };
            #endregion
        }
        
    }
}
