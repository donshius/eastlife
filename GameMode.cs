using SampSharp.GameMode;
using System;
using System.Collections.Generic;
using eastLIFEnet.Controllers;
using eastLIFEnet.World;
using SampSharp.GameMode.Controllers;

namespace eastLIFEnet
{
    public class GameMode : BaseMode
    {
        public static string Name = "EL";
        public static string Version = "0.0.1a";
        
        protected override void OnInitialized(EventArgs e)
        {
            Console.WriteLine("> Initializing gamemode...");

            var database = new Database();
            if (database.Connect("127.0.0.1", "root", "", "eastlife"))
            {
                Console.WriteLine("Connected to MySQL successfully");
            }
            else
            {
                base.OnInitialized(e);
                return;
            }

            DisableInteriorEnterExits();
            EnableStuntBonusForAll(false);
            SetGameModeText($"{Name} v{Version}");
            BusinessResource.Init();
            Entrance.LoadAll();
            Business.LoadAll();
            House.LoadAll();

            Objects.Create();

            Timers.Initialize();

            Job.InitJobs();

            base.OnInitialized(e);
        }

        protected override void LoadControllers(ControllerCollection controllers)
        {
            base.LoadControllers(controllers);
            controllers.Override(new PlayerController());
            controllers.Override(new VehicleController());
            controllers.Override(new ECommandController());
        }

        protected override void OnExited(EventArgs e)
        {
            Business.SaveAll();
            if(Database.GetInstance().IsConnected())
                Database.GetInstance().Close();

            base.OnExited(e);
        }
    }
}