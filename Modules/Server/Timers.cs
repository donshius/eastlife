﻿using System;
using SampSharp.GameMode.SAMP;

namespace eastLIFEnet.World
{
    public class Timers
    {
        public static Timer hourTimer;
        public static Timer minuteTimer;

        public static void Initialize()
        {
            hourTimer = new Timer(1000 * 60 * 60, true);
            hourTimer.Tick += HourTick;

            minuteTimer = new Timer(1000 * 60, true);
            minuteTimer.Tick += MinuteTick;
        }

        public static void HourTick(object sender, EventArgs e)
        {
            foreach(var business in Business.businesses)
            {
                business.CurrentProfit += business.ProfitPerHour;
                business.Save();
            }
        }

        public static void MinuteTick(object sender, EventArgs e)
        {
            foreach (var house in House.Houses)
            {
                house.CheckRent();
            }
        }
    }
}