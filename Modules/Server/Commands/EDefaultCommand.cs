﻿using System.Linq;
using System.Reflection;
using eastLIFEnet.World;
using SampSharp.GameMode.SAMP.Commands;
using SampSharp.GameMode.SAMP.Commands.PermissionCheckers;
using SampSharp.GameMode.World;

namespace eastLIFEnet.Modules.Server.Commands
{
    public class EDefaultCommand
    {
        public string Name { get; }
        public MethodInfo Method { get; set; }
        public bool IsMethodMemberOfPlayer { get; }

        public EDefaultCommand(string name, MethodInfo method)
        {
            Name = name;
            Method = method;
            /* Check whether the method is declared within the main Player class */
            IsMethodMemberOfPlayer = typeof(Player).IsAssignableFrom(method.DeclaringType);
        }

        public static bool HasArguments(string commandText)
        {
            return commandText.Split(' ').Length > 0;
        }

        public static string[] GetArguments(string commandText, bool includeCommandName = false)
        {
            string[] arguments = null;
            if (HasArguments(commandText))
            {
                string[] argArr = commandText.Split(' ');
                if (includeCommandName)
                    return argArr;

                arguments = new string[argArr.Length - ((includeCommandName) ? 0 : 1)];
                for (int i = 1; i < argArr.Length; i++)
                {
                    arguments[i - ((includeCommandName) ? 0 : 1)] = argArr[i];
                }
            }
            return arguments;
        }

        public static string GetArgumentString(string commandText)
        {
            if (HasArguments(commandText))
                return string.Join(" ", GetArguments(commandText));
            return null;
        }

        public bool CanInvoke(Player player, string commandText)
        {
            commandText = commandText.TrimStart('/');
            string[] arguments = GetArguments(commandText, true);

            if (arguments[0].Equals(Name))
                return true;
            return false;
        }

        public bool Invoke(Player player, string commandText)
        {
            return Method.Invoke(IsMethodMemberOfPlayer ? player : null,
                IsMethodMemberOfPlayer ? new object[] {GetArgumentString(commandText)}
                : new object[] {player, GetArgumentString(commandText)}) as bool? ?? true;
        }
    }
}