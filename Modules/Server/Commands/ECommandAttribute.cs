﻿using System;

namespace eastLIFEnet.Modules.Server.Commands
{
    [AttributeUsage(AttributeTargets.Method)]
    public class ECommandAttribute : Attribute
    {
        public string[] Names { get; }
        public Type PermissionChecker { get; set; }

        public ECommandAttribute(string name) : this(new[] {name})
        {
        }

        public ECommandAttribute(params string[] names)
        {
            Names = names;
        }
    }
}