﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using eastLIFEnet.Commands;
using eastLIFEnet.World;
using SampSharp.GameMode;
using SampSharp.GameMode.SAMP.Commands;
using SampSharp.GameMode.World;

namespace eastLIFEnet.Modules.Server.Commands
{
    class ECommandManager : CommandsManager
    {
        public new BaseMode GameMode { get; }
        private List<EDefaultCommand> _commands = new List<EDefaultCommand>();

        public ECommandManager(BaseMode gameMode) : base(gameMode)
        {
            GameMode = gameMode;
        }

        public void RegisterCommands<T>() where T : class
        {
            RegisterCommands(typeof(T));
        }

        public void RegisterCommands(Type typeInAssembly)
        {
            if(typeInAssembly == null) throw new ArgumentNullException(nameof(typeInAssembly));
            RegisterCommands(typeInAssembly.Assembly);
        }

        public void RegisterCommands(Assembly assembly)
        {
            /* Loop through each class in a given assembly */
            foreach (var method in assembly.GetTypes()
                /* Select non-interface and non-abstract classes */
                .Where(type => !type.IsInterface && type.IsClass && !type.IsAbstract)
                /* Select all of the methods */
                .SelectMany(type => type.GetMethods(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static | BindingFlags.Instance))
                /* Select only methods that return a bool */
                .Where(method => method.ReturnType == typeof(bool))
                .Where(method => method.Name.StartsWith("COMMAND_") || method.Name.StartsWith("CMD_"))
            )
            {
                var commandName = method.Name.StartsWith("COMMAND_") ? method.Name.Substring(8, method.Name.Length - 8)
                    : method.Name.Substring(4, method.Name.Length - 4);

                var command = new EDefaultCommand(commandName, method);
                Register(command);
            }
        }

        public void Register(EDefaultCommand command)
        {
            _commands.Add(command);
        }

        public EDefaultCommand GetCommandFromText(Player player, string text)
        {
            foreach (var command in _commands)
            {
                if (command.CanInvoke(player, text))
                    return command;
            }
            return null;
        }

        public bool Process(string commandText, Player player)
        {
            Console.WriteLine("Got command as " + commandText + " from player " + player.Name);
            if (string.IsNullOrWhiteSpace(commandText)) return false;

            var command = GetCommandFromText(player, commandText);

            return command != null && command.Invoke(player, commandText);
        }
    }
}
