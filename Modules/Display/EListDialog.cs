﻿using SampSharp.GameMode.Display;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using eastLIFEnet.Util;

namespace eastLIFEnet.Display
{
    class EListDialog : ListDialog
    {
        public EListDialog(string caption, string button1, string button2 = null) : base(caption, button1, button2)
        {
        }

        public new EListDialog AddItem(string item)
        {
            base.AddItem($"{{{Util.Color.HEX_DEEPSKYBLUE}}}» \t{{{Util.Color.HEX_WHITE}}}{item}");
            return this;
        }
    }
}
