﻿using SampSharp.GameMode;
using SampSharp.GameMode.SAMP;
using SampSharp.GameMode.World;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eastLIFEnet
{
    class Entrance
    {
        public static List<Entrance> entrances = new List<Entrance>();

        private Vector3 m_EntrancePosition;
        private Vector3 m_InteriorPosition;
        private int m_InteriorID;
        private string m_Name;
        private TextLabel m_Label = null;
        private Pickup m_Pickup = null;

        public Entrance(string name, Vector3 entPos, Vector3 intPos, int interior)
        {
            m_EntrancePosition = entPos;
            m_InteriorPosition = intPos;
            m_InteriorID = interior;
            m_Name = name;

            UpdateWorldObjects();
        }

        public void UpdateWorldObjects()
        {
            if (m_Label != null)
                m_Label.Dispose();

            if (m_Pickup != null)
                m_Label.Dispose();

            m_Label = new TextLabel(string.Format("[ {{{0}}}{1} {{{2}}}]\n\nNorėdami įeiti spauskite [ {{{3}}}ENTER {{{4}}}]",
                Util.Color.HEX_AQUA, m_Name, Util.Color.HEX_WHITE, Util.Color.HEX_ORANGE, Util.Color.HEX_WHITE),
                Color.White, m_EntrancePosition, 5.0f, 0, true);

            m_Pickup = Pickup.Create(1318, 1, m_EntrancePosition);
        }
        
        public Vector3 GetEntrancePosition()
        {
            return m_EntrancePosition;
        }

        public Vector3 GetInteriorPosition()
        {
            return m_InteriorPosition;
        }

        public int GetInteriorID()
        {
            return m_InteriorID;
        }

        public static void LoadAll()
        {
            Console.WriteLine("> Loading entrances...");
            Console.WriteLine("> Found {0} entrances...", DBQuery.GetNumberRowsInTable("entrances"));

            DBQuery query = new DBQuery("SELECT * FROM `entrances`");
            query.Execute();

            if(query.GetResult().HasRows)
            {
                while(query.GetResult().Read())
                {
                    Vector3 entrancePosition = new Vector3(query.GetResult().GetFloat("ent_posX"), query.GetResult().GetFloat("ent_posY"), query.GetResult().GetFloat("ent_posZ"));
                    Vector3 interiorPosition = new Vector3(query.GetResult().GetFloat("int_posX"), query.GetResult().GetFloat("int_posY"), query.GetResult().GetFloat("int_posZ"));
                    Entrance entrance = new Entrance(query.GetResult().GetString("name"), entrancePosition, interiorPosition, query.GetResult().GetInt32("interior"));
                    entrances.Add(entrance);
                }
            }

            query.Close();
        }
    }
}
