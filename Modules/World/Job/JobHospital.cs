﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SampSharp.GameMode;

namespace eastLIFEnet
{
    public class JobHospital : Job
    {
        public JobHospital()
        {
            Name = "Ligonine";
            RequiresInvite = true;
            HasRanks = true;
            RequiredScore = 0;
            CreateJoinPickup(new Vector3(1224.5486, 290.6939, 19.5310));
            ID = Register(this);
        }

        protected override void JoinEventHandler(object sender, Player player)
        {
            player.SendSuccessMessage("Sėkmingai įsidarbinote ligoninei TODO bbz");
        }

        public static bool CMD_invitem(Player player, string ps)
        {
            if (!player.JobInvites.Contains(Hospital.ID)) player.JobInvites.Add(Hospital.ID);
            player.SendSuccessMessage("Gavote JobHospital pakvietima");
            return true;
        }
    }
}
