﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eastLIFEnet
{
    public class JobRanks
    {
        public const string Table = "jobranks";
        public const int NameLimit = 30;
        public int RankLimit;
        //                sqlid, name
        public List<Tuple<int, string>> ranks = new List<Tuple<int, string>>();
        private int JobId;

        public JobRanks(int jobid, int ranklimit = 5)
        {
            JobId = jobid;
            RankLimit = ranklimit;
            Load();
            Console.WriteLine("Created jobranks instance for job id " + jobid + " with " + ranks.Count + " ranks");
        }

        public static string GetName(int id)
        {
            string retVal = null;
            foreach (var job in Job.Jobs)
            {
                foreach (var rank in job.Ranks.ranks)
                {
                    if (rank.Item1 == id)
                    {
                        retVal = rank.Item2;
                        break;
                    }
                }
            }
            return retVal;
        }

        public void Edit(int id, string newName)
        {
            var foundRank = ranks.FindIndex(t => t.Item1 == id);
            if (foundRank != -1)
            {
                ranks[foundRank] = new Tuple<int, string>(id, newName);
            }
        }

        public void DeleteByIndex(int index)
        {
            new DBQuery($"DELETE FROM `{Table}` WHERE id='{ranks[index].Item1}'", QueryType.INSERT_OR_DELETE).Execute();
            foreach (var player in Player.All)
            {
                if (((Player) player).JobRank != ranks[index].Item1) continue;
                ((Player) player).JobRank = 0;
            }
            new DBQuery($"UPDATE `{Player.Table}` SET jobRank='0' WHERE jobRank='{ranks[index].Item1}'", QueryType.INSERT_OR_DELETE).Execute();
            ranks.RemoveAt(index);
        }

        public void Delete(int id)
        {
            var foundRank = ranks.FindIndex(t => t.Item1 == id);
            if(foundRank != -1)
                ranks.RemoveAt(foundRank);
        }

        public int Add(string name)
        {
            var query = new DBQuery($"INSERT INTO `{Table}` (name, jobId) VALUES (@name, '{JobId}')", QueryType.INSERT_OR_DELETE);
            query.AddParam("@name", name);
            query.Execute();
            ranks.Add(new Tuple<int, string>((int)query.GetCommand().LastInsertedId, name));
            return ranks.FindIndex(t => t.Item1 == query.GetCommand().LastInsertedId);
        }

        private void Load()
        {
            ranks.Clear();

            var query = new DBQuery($"SELECT * FROM `{Table}` WHERE jobId='{JobId}'", QueryType.QUERY);
            int rowCount = 0;
            query.Execute();
            if (query.GetResult().HasRows)
            {
                while (query.GetResult().Read())
                {
                    rowCount++;
                    ranks.Add(new Tuple<int, string>(query.GetResult().GetInt32("id"), query.GetResult().GetString("name")));
                }
            }
            query.Close();
            if (rowCount > RankLimit)
            {
                Console.WriteLine($"> Warning: Job {Job.Jobs[JobId - 1].Name} has exceeded rank limit. Current: {rowCount} Max: {RankLimit}");
            }
        }

        public static JobRanks Create(int jobid, int ranklimit = 5)
        {
            return new JobRanks(jobid, ranklimit);
        }
    }
}
