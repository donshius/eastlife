﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExtensionMethods;
using SampSharp.GameMode;
using SampSharp.GameMode.Events;
using SampSharp.GameMode.SAMP;
using SampSharp.GameMode.World;

namespace eastLIFEnet
{
    public abstract class Job
    {
        public static List<Job> Jobs = new List<Job>();

        public static JobPolice Police;
        public static JobHospital Hospital;

        protected int RequiredScore = 0;
        public string Name = "";
        protected bool RequiresInvite = false;
        public int ID = 0;
        protected Vector3 JoinPickupPosition;
        protected Pickup JoinPickup;
        protected EventHandler<Player> JoinEvent = null;
        protected bool HasRanks = false;
        public JobRanks Ranks;
        public JobAssistantPermissions AssistantPermissions;

        protected Job()
        {

        }

        protected void CreateJoinPickup(Vector3 position)
        {
            JoinPickupPosition = position;
            JoinPickup = Pickup.Create(1275, 2, position);
            JoinPickup.PickUp += JoinPickupHandler;
        }

        protected abstract void JoinEventHandler(object sender, Player player);

        protected virtual void JoinPickupHandler(object sender, PlayerEventArgs a)
        {
            var player = a.Player as Player;
            if (player.Job != null)
            {
                player.SendErrorMessage("Jūs jau esate įsidarbine.");
                return;
            }
            Console.WriteLine("Job id " + ID);
            if (RequiresInvite)
            {
                if (!player.JobInvites.Contains(ID))
                {
                    player.SendErrorMessage("Pakvietimas yra reikalingas šiam darbui.");
                    return;
                }
            }
            if (RequiredScore > player.Score)
            {
                player.SendErrorMessage($"Jūs neturite pakankamai patirties. Reikalinga: {Color.Silver.RGB()}{RequiredScore}");
                return;
            }
            JoinEvent?.Invoke(this, player);
            player.Job = this;
            /*
             SAVE PLAYER INFO UPON JOIN
             */
        }

        protected static int Register(Job job, int ranklimit = 5)
        {
            if(!Jobs.Contains(job)) Jobs.Add(job);
            job.JoinEvent += job.JoinEventHandler;

            job.AssistantPermissions = new JobAssistantPermissions(Jobs.Count);
            job.AssistantPermissions.Load();

            job.Ranks = JobRanks.Create(Jobs.Count, ranklimit);

            Console.WriteLine($"> Created job '{job.Name}'");
            return Jobs.Count;
        }

        public static Job Find(int id)
        {
            return id == 0 ? null : Jobs.Find(j => j.ID == id);
        }

        public static void InitJobs()
        {
            Police = new JobPolice();
            Hospital = new JobHospital();
        }
    }
}
