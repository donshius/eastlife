﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eastLIFEnet
{
    public class JobAssistantPermissions
    {
        public static string Table = "jobpermissions";
        public static int CAN_HIRE_EMPLOYEES = 0;
        public static int CAN_FIRE_EMPLOYEES = 1;
        public static int CAN_EDIT_RANKS = 2;
        public static int CAN_CREATE_RANKS = 3;
        public static int CAN_CHANGE_EMPLOYEE_RANK = 4;
        public static int CAN_DELETE_RANKS = 5;

        public List<Tuple<string, bool>> permissions = new List<Tuple<string, bool>>()
        {
            new Tuple<string, bool>("Gali samdyti darbuotojus", false),
            new Tuple<string, bool>("Gali atleisti darbuotojus", false),
            new Tuple<string, bool>("Gali redaguoti rangus", false),
            new Tuple<string, bool>("Gali kūrti rangus", false),
            new Tuple<string, bool>("Gali kesti žaidėjo rangą", false),
            new Tuple<string, bool>("Gali trinti rangus", false)
        };

        private int JobId;
        private int Sqlid;

        public JobAssistantPermissions(int jobid)
        {
            JobId = jobid;
        }

        public static bool CheckPermission(Player player, int index)
        {
            return player.JobAssistant.AssistantPermissions.permissions[index].Item2;
        }

        public static string GetDeniedMessage()
        { return "Jūs neturite leidimo tai daryti."; }

        public void Load()
        {
            Console.WriteLine("> Loading assistant permissions for job " + Job.Jobs[JobId-1].Name);
            var query = new DBQuery($"SELECT * FROM `{Table}` WHERE jobId='{JobId}' LIMIT 1", QueryType.QUERY);
            query.Execute();
            if (query.GetResult().HasRows)
            {
                query.GetResult().Read();
                string[] permArr = query.GetResult().GetString("permissions").Split('|');
                foreach (var permission in permArr)
                {
                    var permValueArr = permission.Split(':');
                    if (int.TryParse(permValueArr[0], out int permIndex))
                    {
                        permissions[permIndex] = new Tuple<string, bool>(permissions[permIndex].Item1, Convert.ToBoolean(Convert.ToInt32(permValueArr[1])));
                    }
                }
                Sqlid = query.GetResult().GetInt32("id");
            }
            query.Close();
        }

        public void Save()
        {
            // Save format: index:val|index:val
            string saveStr = "";
            for (int i = 0; i < permissions.Count; i++)
            {
                if (i > 0) saveStr += '|';
                saveStr += ""+i + ':' + ((permissions[i].Item2) ? 1 : 0);
            }
            Console.WriteLine("Save str " + saveStr);
            new DBQuery($"UPDATE `{Table}` SET permissions='{saveStr}' WHERE id='{Sqlid}'", QueryType.INSERT_OR_DELETE).Execute();
        }
    }
}
