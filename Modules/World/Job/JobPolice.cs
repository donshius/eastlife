﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SampSharp.GameMode;

namespace eastLIFEnet
{
    public class JobPolice : Job
    {
        public JobPolice()
        {
            RequiredScore = 0;
            RequiresInvite = true;
            HasRanks = true;
            Name = "Policija";
            CreateJoinPickup(new Vector3(661.0400, -546.6511, 16.3360));
            ID = Register(this);
        }

        protected override void JoinEventHandler(object sender, Player player)
        {
            player.SendSuccessMessage("Sėkmingai įsidarbinote policijoje TODO bbz");
        }

        public static bool CMD_invite(Player player, string ps)
        {
            if(!player.JobInvites.Contains(Police.ID)) player.JobInvites.Add(Police.ID);
            player.SendSuccessMessage("Gavote pakvietima");
            return true;
        }
    }
}
