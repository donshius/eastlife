﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using eastLIFEnet.Display;
using ExtensionMethods;
using MySql.Data.MySqlClient;
using SampSharp.GameMode.Definitions;
using SampSharp.GameMode.Display;
using SampSharp.GameMode.SAMP;

namespace eastLIFEnet
{
    public static class JobUI
    {
        public static void LeaderDialog(Player player, Job job, bool isAssistant = false)
        {
            Console.WriteLine("Create dialog");
            var dialog = new EListDialog($"{((isAssistant) ? "Pavaduotojaus" : "Direktoriaus")} meniu ({job.Name})", "Rinktis", "Išeiti");
            Console.WriteLine("Add items");
            dialog.AddItem("Pakviesti į darbą");
            dialog.AddItem("Darbuotojai");
            dialog.AddItem("Rangai");
            if(!isAssistant)
                dialog.AddItem("Pavaduotojo leidimai");
            Console.WriteLine("Resposne");
            dialog.Response += (sender, e) =>
            {
                if (e.DialogButton != DialogButton.Left) return;
                if (e.ListItem == 0)
                {
                    LeaderInviteDialog(player, job, isAssistant);
                }
                else if (e.ListItem == 1)
                {
                    LeaderEmployeesDialog(player, job, isAssistant);
                }
                else if (e.ListItem == 2)
                {
                    LeaderRanksDialog(player, job, isAssistant);
                }
                else if (e.ListItem == 3 && !isAssistant)
                {
                    LeaderAssistantPermissions(player);
                }
            };

            dialog.Show(player);
        }

        private static void LeaderInviteDialog(Player player, Job job, bool isAssistant = false)
        {
            var dialog = new InputDialog("Pakvietimas į darbą",
                    $"{Color.White.RGB()}Norėdami pakviesti žaidėja į šį darbą įrašykite jo vardą:\n" +
                    $"Jeigu žaidėjas nėra serveryje, įrašykite jo pilną vardą.",
            false, "Pakviesti", "Atgal");

            if (isAssistant &&
                !JobAssistantPermissions.CheckPermission(player, JobAssistantPermissions.CAN_HIRE_EMPLOYEES))
            {
                player.SendErrorMessage(JobAssistantPermissions.GetDeniedMessage());
                return;
            }

            dialog.Response += (sender, e) =>
            {
                if (e.DialogButton == DialogButton.Right)
                {
                    LeaderDialog(player, job, isAssistant);
                    return;
                }

                var target = Player.Get(e.InputText);
                if (target == null)
                {
                    /* Attempt offline search */
                    var query = new DBQuery($"SELECT * FROM `{Player.Table}` WHERE name=@Name LIMIT 1", QueryType.QUERY);
                    query.AddParam("@Name", e.InputText).Execute();
                    if (query.GetResult().HasRows)
                    {
                        if (query.GetResult().Read())
                        {
                            bool isAlreadyInvited = false;
                            var currentJobInvites = query.GetResult().GetString("jobInvites");
                            var currentJobInviteArr = currentJobInvites.Split('|');
                            foreach (var jobInvite in currentJobInviteArr)
                            {
                                if (int.TryParse(jobInvite, out int jobInviteParsed))
                                {
                                    if (jobInviteParsed == player.JobLeader.ID)
                                    {
                                        isAlreadyInvited = true;
                                        break;
                                    }
                                }
                            }
                            if (isAlreadyInvited)
                            {
                                player.SendErrorMessage("Šis žaidėjas jau yra pakviestas į šį darbą!");
                                query.Close();
                            }
                            else
                            {
                                currentJobInvites += (currentJobInvites.Length > 0) ? ("|" + player.JobLeader.ID) : "" + player.JobLeader.ID;
                                query.Close();

                                query = new DBQuery($"UPDATE `{Player.Table}` SET jobInvites=@jobInvites WHERE name=@Name", QueryType.INSERT_OR_DELETE);
                                query.AddParam("@jobInvites", currentJobInvites)
                                    .AddParam("@Name", e.InputText)
                                    .Execute();
                                player.SendSuccessMessage($"Žaidėjas {Color.Silver.RGB()}{e.InputText} {Color.White.RGB()}sėkmingai pakviestas.");
                            }
                        }
                        else
                        {
                            query.Close();
                        }
                    }
                    else
                    {
                        player.SendErrorMessage("Toks žaidėjas nerastas.");
                        LeaderInviteDialog(player, job, isAssistant);
                        query.Close();
                    }
                }
                else
                {
                    if (!target.JobInvites.Contains(job.ID))
                    {
                        player.SendSuccessMessage($"Žaidėjas {Color.Silver.RGB()}{target.Name} {Color.White.RGB()}sėkmingai pakviestas.");
                        target.JobInvites.Add(job.ID);
                        target.SendInfoMessage($"Direktorius {Color.Silver.RGB()}{player.Name} {Color.White.RGB()}" +
                                               $"jūs pakvietė į šį darbą: {Color.Aqua.RGB()}{job.Name}");
                    }
                    else
                    {
                        player.SendErrorMessage("Šis žaidėjas jau yra pakviestas į šį darbą!");
                    }
                }
            };

            dialog.Show(player);
        }

        private static void LeaderEmployeesDialog(Player player, Job job, bool isAssistant = false)
        {
            var dialog = new EListDialog("Darbuotojai", "Rinktis", "Atgal");
            var query = new DBQuery($"SELECT * FROM `{Player.Table}` WHERE job=@job AND name!=@Name", QueryType.QUERY);
            query.AddParam("@job", job.ID).AddParam("@Name", player.Name);
            query.Execute();
            
            var employeeNames = new List<Tuple<string, int>>();
            if (query.GetResult().HasRows)
            {
                while (query.GetResult().Read())
                {
                    var targetName = query.GetResult().GetString("name");
                    var target = Player.Get(targetName);
                    dialog.AddItem($"{targetName} - {((target == null) ? $"{Color.Orange.RGB()}Atsijungęs" : $"{Color.Lime.RGB()}Prisijungęs")}");
                    employeeNames.Add(new Tuple<string, int>(targetName, target?.JobRank ?? query.GetResult().GetInt32("jobRank")));
                }
            }

            query.Close();
            
            dialog.Response += (sender, e) =>
            {
                if (e.DialogButton == DialogButton.Right)
                {
                    LeaderDialog(player, job, isAssistant);
                    return;
                }
                if (employeeNames.Count <= 0) return;
                player.SendInfoMessage("Clicked on " + employeeNames[e.ListItem]);
                LeaderEmployeeControlDialog(player, employeeNames[e.ListItem], job, isAssistant);
            };

            dialog.Show(player);
        }

        private static void LeaderEmployeeControlDialog(Player player, Tuple<string, int> employeeData, Job job, bool isAssistant = false)
        {
            var isEmployeeOnline = (Player.Get(employeeData.Item1) != null);
            var dialog = new EListDialog($"Darbuotojo '{employeeData.Item1}' valdymas", "Rinktis", "Atgal");

            dialog.AddItem($"Rangas: {Color.Aqua.RGB()}{((JobRanks.GetName(employeeData.Item2) == null) ? $"{Color.Orange.RGB()}nėra" : JobRanks.GetName(employeeData.Item2))}");
            dialog.AddItem("Darbuotojo statistika");
            dialog.AddItem("Išmesti darbuotoją");

            dialog.Response += (sender, e) =>
            {
                if (e.DialogButton == DialogButton.Right)
                {
                    LeaderEmployeesDialog(player, job, isAssistant);
                    return;
                }
                if (e.ListItem == 0) // Keisti/nuimti/uzdeti ranka
                {
                    LeaderEmployeeControlRank(player, employeeData, job, isAssistant);
                }
                else if (e.ListItem == 1) // stats
                {
                    // TODO stats
                }
                else if (e.ListItem == 2) // Ismesti
                {
                    LeaderEmployeeControlKickDialog(player, employeeData, job, isAssistant);
                }
            };

            dialog.Show(player);
        }

        private static void LeaderEmployeeControlKickDialog(Player player, Tuple<string, int> employeeData, Job job, bool isAssistant = false)
        {
            if (isAssistant &&
                !JobAssistantPermissions.CheckPermission(player, JobAssistantPermissions.CAN_FIRE_EMPLOYEES))
            {
                player.SendErrorMessage(JobAssistantPermissions.GetDeniedMessage());
                return;
            }
            var dialog = new MessageDialog($"'{employeeData.Item1}' išmetimas",
                    $"{Color.White.RGB()}Jūs pasirinkote išmesti darbuotoją {employeeData.Item1} iš šio darbo.\n\n" +
                    $"Ar tikrai norite išmesti šį darbuotoją?",
            "Išmesti", "Atgal");

            dialog.Response += (sender, e) =>
            {
                if (e.DialogButton == DialogButton.Right)
                {
                    LeaderEmployeeControlDialog(player, employeeData, job, isAssistant);
                    return;
                }
                var target = Player.Get(employeeData.Item1);

                if (target != null)
                {
                    if (target.Job.ID == job.ID)
                    {
                        target.Job = null;
                        target.SendInfoMessage($"Jūs buvote išmestas iš šio darbo: {Color.Aqua.RGB()}{job.Name}");
                    }
                }
                else
                {
                    var query = new DBQuery($"UPDATE `{Player.Table}` SET job='0' WHERE name='{employeeData.Item1}' AND job='{job.ID}'", QueryType.INSERT_OR_DELETE);
                    query.Execute();
                }
                player.SendSuccessMessage($"Žaidėjas {Color.Aqua.RGB()}{employeeData.Item1} {Color.White.RGB()}buvo išmestas iš darbo.");
            };

            dialog.Show(player);
        }

        private static void LeaderRanksDialog(Player player, Job job, bool isAssistant = false)
        {
            var dialog = new EListDialog($"Rangu valdymas ({job.Name})", "Rinktis", "Atgal");

            bool isUnderLimit = job.Ranks.ranks.Count < job.Ranks.RankLimit;

            if (isUnderLimit)
            {
                dialog.AddItem("Pridėti rangą");
            }

            for (int i = 0; i < job.Ranks.RankLimit; i++)
            {
                dialog.AddItem($"{i + 1}. {Color.Aqua.RGB()}{(job.Ranks.ranks.ElementAtOrDefault(i)?.Item2 ?? Color.Orange.RGB() + "tuščia")}");
            }

            dialog.Response += (sender, e) =>
            {
                if (e.DialogButton == DialogButton.Right)
                {
                    LeaderDialog(player, job, isAssistant);
                    return;
                }

                if (isUnderLimit)
                {
                    if (e.ListItem == 0) // Prideti
                    {
                        LeaderRanksAddDialog(player, job, isAssistant);
                        return;
                    }
                }
                int elementIdOffset = (isUnderLimit) ? 1 : 0;
                if (job.Ranks.ranks.ElementAtOrDefault(e.ListItem - elementIdOffset) == null)
                {
                    LeaderRanksAddDialog(player, job, isAssistant);
                }
                else
                {
                    LeaderRanksEditDialog(player, e.ListItem - elementIdOffset, job, isAssistant);
                }
            };

            dialog.Show(player);
        }

        private static void LeaderRanksAddDialog(Player player, Job job, bool isAssistant = false)
        {
            if (isAssistant &&
                !JobAssistantPermissions.CheckPermission(player, JobAssistantPermissions.CAN_CREATE_RANKS))
            {
                player.SendErrorMessage(JobAssistantPermissions.GetDeniedMessage());
                return;
            }
            var dialog = new InputDialog("Rango pridėjimas", 
                    $"{Color.White.RGB()}Norėdami pridėti naują rangą, įveskite pavadinimą:\n" +
                    $"{Color.White.RGB()}Max rango pavadinimo ilgis: {Color.Gold.RGB()}{JobRanks.NameLimit}",
            false, "Pridėti", "Atgal");

            dialog.Response += (sender, e) =>
            {
                if (e.DialogButton == DialogButton.Right)
                {
                    LeaderRanksDialog(player, job, isAssistant);
                    return;
                }
                if (e.InputText.Length < 3)
                {
                    player.SendErrorMessage("Rangas privalo turėti min. 3 charakterius.");
                    LeaderRanksAddDialog(player, job, isAssistant);
                    return;
                }
                if (e.InputText.Length > JobRanks.NameLimit)
                {
                    player.SendErrorMessage($"Maksimalus charakterių skaičius rango pavadinime yra {Color.Aqua.RGB()}{JobRanks.NameLimit}");
                    LeaderRanksAddDialog(player, job, isAssistant);
                    return;
                }

                job.Ranks.Add(e.InputText);
                player.SendSuccessMessage("Rangas sėkmingai pridėtas.");
            };

            dialog.Show(player);
        }

        private static void LeaderRanksEditDialog(Player player, int rankIndex, Job job, bool isAssistant = false)
        {
            var dialog = new EListDialog($"Rango '{job.Ranks.ranks.ElementAt(rankIndex).Item2}' valdymas", "Rinktis", "Atgal");
            dialog.AddItem("Pervadinti rangą");
            dialog.AddItem("Ištrinti rangą");

            dialog.Response += (sender, e) =>
            {
                if (e.DialogButton == DialogButton.Right)
                {
                    LeaderRanksDialog(player, job, isAssistant);
                    return;
                }
                if (e.ListItem == 0)
                {
                    LeaderRanksEditRename(player, rankIndex, job, isAssistant);
                }
                else if (e.ListItem == 1)
                {
                    LeaderRanksEditDelete(player, rankIndex, job, isAssistant);
                }
            };

            dialog.Show(player);
        }

        private static void LeaderRanksEditRename(Player player, int rankIndex, Job job, bool isAssistant = false)
        {
            if (isAssistant &&
                !JobAssistantPermissions.CheckPermission(player, JobAssistantPermissions.CAN_EDIT_RANKS))
            {
                player.SendErrorMessage(JobAssistantPermissions.GetDeniedMessage());
                return;
            }
            var dialog = new InputDialog($"Rango '{job.Ranks.ranks[rankIndex].Item2}' pervadinimas",
                    $"{Color.White.RGB()}Jūs pasirinkote pervadinti rangą {Color.Aqua.RGB()}'{job.Ranks.ranks.ElementAt(rankIndex).Item2}'.\n" +
                    $"{Color.White.RGB()}Įveskite nauja rango pavadinimą:",
            false, "Gerai", "Atgal");

            dialog.Response += (sender, e) =>
            {
                if (e.DialogButton == DialogButton.Right)
                {
                    LeaderRanksEditDialog(player, rankIndex, job, isAssistant);
                    return;
                }
                if (e.InputText.Length < 3)
                {
                    player.SendErrorMessage("Rangas privalo turėti min. 3 charakterius.");
                    LeaderRanksEditRename(player, rankIndex, job, isAssistant);
                    return;
                }
                if (e.InputText.Length > JobRanks.NameLimit)
                {
                    player.SendErrorMessage($"Maksimalus charakterių skaičius rango pavadinime yra {Color.Aqua.RGB()}{JobRanks.NameLimit}");
                    LeaderRanksEditRename(player, rankIndex, job, isAssistant);
                    return;
                }
                job.Ranks.ranks[rankIndex] = new Tuple<int, string>(job.Ranks.ranks[rankIndex].Item1, e.InputText);
                player.SendSuccessMessage("Rangas sėkmingai pervadintas.");
            };

            dialog.Show(player);
        }

        private static void LeaderRanksEditDelete(Player player, int rankIndex, Job job, bool isAssistant = false)
        {
            if (isAssistant &&
                !JobAssistantPermissions.CheckPermission(player, JobAssistantPermissions.CAN_DELETE_RANKS))
            {
                player.SendErrorMessage(JobAssistantPermissions.GetDeniedMessage());
                return;
            }
            var dialog = new MessageDialog($"Rango '{job.Ranks.ranks[rankIndex].Item2}' ištrinimas",
                    $"{Color.White.RGB()}Jūs pasirinkote ištrinti rangą {Color.Aqua.RGB()}'{job.Ranks.ranks[rankIndex].Item2}'\n" +
                    $"{Color.White.RGB()}Ar tikrai norite ištrinti šį rangą?",
            "Trinti", "Atgal");

            dialog.Response += (sender, e) =>
            {
                if (e.DialogButton == DialogButton.Right)
                {
                    LeaderRanksEditDialog(player, rankIndex, job, isAssistant);
                    return;
                }
                // todo jeigu bus pavaduotojai tikrinti ar vis dar egzsituoja
                player.SendSuccessMessage($"Rangas {Color.Aqua.RGB()}'{job.Ranks.ranks[rankIndex].Item2}' {Color.White.RGB()}sėkmingai ištrintas.");
                job.Ranks.DeleteByIndex(rankIndex);
            };

            dialog.Show(player);
        }

        private static void LeaderEmployeeControlRank(Player player, Tuple<string, int> employeeData, Job job, bool isAssistant = false)
        {
            var dialog = new EListDialog($"Darbuotojo '{employeeData.Item1}' rangas", "Rinktis", "Atgal");

            var target = Player.Get(employeeData.Item1);
            int currentTargetRank;

            if (target != null)
            {
                currentTargetRank = target.JobRank;
            }
            else
            {
                var query = new DBQuery($"SELECT * FROM `{Player.Table}` WHERE name='{employeeData.Item1}' LIMIT 1", QueryType.QUERY);
                query.Execute(true);
                if (!query.GetResult().HasRows)
                {
                    player.SendErrorMessage($"Žaidėjas {Color.Aqua.RGB()}{employeeData.Item1} {Color.White.RGB()}nerastas.");
                    query.Close();
                    return;
                }
                currentTargetRank = query.GetResult().GetInt32("jobRank");
                query.Close();
            }
            var targetHasRank = currentTargetRank > 0;
            dialog.AddItem($"{(targetHasRank ? $"Rangas: {Color.Aqua.RGB()}" + JobRanks.GetName(currentTargetRank) : Color.LimeGreen.RGB() + "Uždėti rangą")}");

            if (targetHasRank)
                dialog.AddItem("Nuimti rangą");

            dialog.Response += (sender, e) =>
            {
                if (e.DialogButton == DialogButton.Right)
                {
                    LeaderEmployeeControlDialog(player, employeeData, job, isAssistant);
                    return;
                }
                if (e.ListItem == 0) // uzdeti arba keisti
                {
                    LeaderEmployeeSetRank(player, employeeData, targetHasRank, job, isAssistant);
                }
                else if (e.ListItem == 1 && targetHasRank) // nuimti
                {
                    if (target != null)
                        target.JobRank = 0;
                    else
                        new DBQuery($"UPDATE `{Player.Table}` SET jobRank='0' WHERE name='{employeeData.Item1}'", QueryType.INSERT_OR_DELETE).Execute();

                    player.SendSuccessMessage($"Rangas sėkmingai nuimtas žaidėjui {employeeData.Item1}");
                }
            };

            dialog.Show(player);
        }

        private static void LeaderEmployeeSetRank(Player player, Tuple<string, int> employeeData, bool targetHasRank, Job job, bool isAssistant = false)
        {
            if (isAssistant &&
                !JobAssistantPermissions.CheckPermission(player, JobAssistantPermissions.CAN_CHANGE_EMPLOYEE_RANK))
            {
                player.SendErrorMessage(JobAssistantPermissions.GetDeniedMessage());
                return;
            }
            var dialog = new EListDialog(((targetHasRank) ? "Rango keitimas" : "Rango uždėjimas"), "Rinktis", "Atgal");

            for (int i = 0; i < job.Ranks.RankLimit; i++)
            {
                var rank = job.Ranks.ranks.ElementAtOrDefault(i);
                if (rank == null) continue;
                dialog.AddItem($"{i + 1}. {Color.Aqua.RGB()}{rank.Item2}");
            }

            dialog.Response += (sender, e) =>
            {
                if (e.DialogButton == DialogButton.Right)
                {
                    LeaderEmployeeControlRank(player, employeeData, job, isAssistant);
                    return;
                }
                var rank = job.Ranks.ranks.ElementAtOrDefault(e.ListItem);
                if (rank == null) return;
               
                var target = Player.Get(employeeData.Item1);
                if (target != null)
                {
                    target.JobRank = rank.Item1;
                    target.SendCustomMessage("Darbas", Color.Azure.RGB(), $"Jūsų rangas buvo pakeistas į {Color.Gold.RGB()}'{rank.Item2}'");
                }
                else
                {
                    new DBQuery($"UPDATE `{Player.Table}` SET jobRank='{rank.Item1}' WHERE name='{employeeData.Item1}'", QueryType.INSERT_OR_DELETE).Execute();
                }
                player.SendSuccessMessage($"Sėkmingai nustatėte darbuotojo {Color.Coral.RGB()}'{employeeData.Item1}' {Color.White.RGB()}rangą į {Color.Coral.RGB()}'{rank.Item2}'");
            };

            dialog.Show(player);
        }

        private static void LeaderAssistantPermissions(Player player)
        {
            var dialog = new EListDialog("Pavaduotojo Leidimai", "Rinktis", "Atgal");
            string assistantName = null;
            var isAssistantOnline = false;
            
            var query = new DBQuery($"SELECT * FROM `{Player.Table}` WHERE jobAssistant='{player.JobLeader.ID}' LIMIT 1", QueryType.QUERY);
            query.Execute();

            if (query.GetResult().HasRows)
            {
                query.GetResult().Read();
                assistantName = query.GetResult().GetString("name");
                isAssistantOnline = Player.Get(assistantName) != null;
            }
            else
            {
                foreach (var target in Player.All)
                {
                    if (((Player) target).JobAssistant?.ID == player.JobLeader.ID)
                    {
                        isAssistantOnline = true;
                        assistantName = target.Name;
                        break;
                    }
                }
            }
            query.Close();
            dialog.AddItem($"Pavaduotojas: {Color.Aqua.RGB()}{assistantName ?? Color.Orange.RGB() + "nėra"} {((assistantName != null) ? (isAssistantOnline ? Color.Lime.RGB() + "(Prisijungęs)" : Color.Orange.RGB() + "(Atsijungęs)") : "")}");

            foreach (var permission in player.JobLeader.AssistantPermissions.permissions)
            {
                dialog.AddItem($"{permission.Item1}: {((permission.Item2) ? Color.Lime.RGB() + "Taip" : Color.Orange.RGB() + "Ne")}");
            }

            dialog.Response += (sender, e) =>
            {
                if (e.DialogButton == DialogButton.Right)
                {
                    LeaderDialog(player, player.JobLeader, false);
                    return;
                }
                if (e.ListItem > 0)
                {
                    LeaderAssistantPermissionEdit(player, e.ListItem-1);
                }
            };

            dialog.Show(player);
        }

        private static void LeaderAssistantPermissionEdit(Player player, int index, bool isAssistant = false)
        {
            var dialog = new EListDialog("Pavaduotojo Leidimas", "Rinktis", "Atgal");

            if (index >= player.JobLeader.AssistantPermissions.permissions.Count)
                return;

            dialog.AddItem($"{player.JobLeader.AssistantPermissions.permissions[index].Item1}: {((player.JobLeader.AssistantPermissions.permissions[index].Item2) ? Color.Lime.RGB() + "Taip" : Color.Orange.RGB() + "Ne")}");
            dialog.AddItem($"{Color.Aqua.RGB()}{((player.JobLeader.AssistantPermissions.permissions[index].Item2) ? "Neleisti" : "Leisti")}");

            dialog.Response += (sender, e) =>
            {
                if (e.DialogButton == DialogButton.Right)
                {
                    LeaderAssistantPermissions(player);
                    return;
                }

                if (e.ListItem == 1)
                {
                    player.JobLeader.AssistantPermissions.permissions[index] = new Tuple<string, bool>(player.JobLeader.AssistantPermissions.permissions[index].Item1,
                            !player.JobLeader.AssistantPermissions.permissions[index].Item2);
                    player.JobLeader.AssistantPermissions.Save();
                    player.SendSuccessMessage("Pavaduotojo leidimas pakeistas.");
                    LeaderAssistantPermissionEdit(player, index);
                }
                
            };

            dialog.Show(player);
        }
    }
}
