﻿using ExtensionMethods;
using SampSharp.GameMode.SAMP;

namespace eastLIFEnet
{
    public class PlayerCommands
    {
        public static bool CMD_get(Player player, string ps)
        {
            if (!ps.sscanf("u", out object[] args))
            {
                player.SendUsageMessage("/get [žaidėjo vardas arba id]");
                return true;
            }
            var target = args[0] as Player;
            if (target == null)
            {
                player.SendErrorMessage("Toks žaidėjas nerastas.");
                return true;
            }
            if (player.Id.Equals(target.Id))
            {
                player.SendErrorMessage("Negalite atsiteleportuoti savęs.");
                return true;
            }
            target.SendCustomMessage("TELEPORT", Color.Aqua.RGB(), $"Žaidėjas {Color.Silver.RGB()}{player.Name} {Color.White.RGB()}nori nuteleportuoti jus pas save.");
            target.SendCustomMessage("TELEPORT", Color.Aqua.RGB(), $"Jeigu norite leisti teleportacija naudokite {Color.LightGreen.RGB()}/tpleisti {Color.White.RGB()}jeigu ne - {Color.Red.RGB()}/tpneleisti");
            target.pendingTeleporter = null;
            target.pendingTeleportTo = player;
            player.SendCustomMessage("TELEPORT", Color.Aqua.RGB(), "Teleportacijos prašymas išsiųstas.");
            return true;
        }

        public static bool CMD_to(Player player, string ps)
        {
            if (!ps.sscanf("u", out object[] args))
            {
                player.SendUsageMessage("/get [žaidėjo vardas arba id]");
                return true;
            }
            var target = args[0] as Player;
            if (target == null)
            {
                player.SendErrorMessage("Toks žaidėjas nerastas.");
                return true;
            }
            if (player.Id.Equals(target.Id))
            {
                player.SendErrorMessage("Pas save teleportuotis negalite.");
                return true;
            }
            target.SendCustomMessage("TELEPORT", Color.Aqua.RGB(), $"Žaidėjas {Color.Silver.RGB()}{player.Name} {Color.White.RGB()}nori atsiteleportuoti pas jus.");
            target.SendCustomMessage("TELEPORT", Color.Aqua.RGB(), $"Jeigu norite leisti teleportacija naudokite {Color.LightGreen.RGB()}/tpleisti {Color.White.RGB()}jeigu ne - {Color.Red.RGB()}/tpneleisti");
            target.pendingTeleportTo = null;
            target.pendingTeleporter = player;
            player.SendCustomMessage("TELEPORT", Color.Aqua.RGB(), "Teleportacijos prašymas išsiųstas.");
            return true;
        }

        public static bool CMD_tpleisti(Player player, string ps)
        {

            return true;
        }

        public static bool CMD_dmeniu(Player player, string ps)
        {
            if (player.JobLeader == null)
            {
                player.SendErrorMessage("Jūs neesate darbo direktorius.");
                return true;
            }
            JobUI.LeaderDialog(player, player.JobLeader, false);
            return true;
        }

        public static bool CMD_pmeniu(Player player, string ps)
        {
            if (player.JobAssistant == null)
            {
                player.SendErrorMessage("Jūs neesate darbo pavaduotojas.");
                return true;
            }
            JobUI.LeaderDialog(player, player.JobAssistant, true);
            return true;
        }
    }
}