﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eastLIFEnet.Modules.World.Player
{
    public class PlayerCache
    {
        public List<Tuple<string, object>> cache = new List<Tuple<string, object>>();

        public PlayerCache() { }

        public void Set(string key, object val)
        {
            if (ContainsKey(key))
            {
                var foundTuple = cache.Find(t => t.Item1 == key);
                var indexOfTuple = cache.IndexOf(foundTuple);
                /* Overwrite existing tuple with a new object */
                cache[indexOfTuple] = new Tuple<string, object>(key, val);
            }
            else
            {
                cache.Add(new Tuple<string, object>(key, val));
            }   
        }

        public object Get(string key)
        {
            return cache.Find(t => t.Item1 == key).Item2;
        }

        public bool ContainsKey(string key)
        {
            return cache.Exists(t => t.Item1 == key);
        }
    }
}
