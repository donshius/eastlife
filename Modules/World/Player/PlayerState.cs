﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eastLIFEnet
{
    [Flags]
    public enum PlayerState
    {
        None = 0,
        LoggedIn = 1,
        Playing = 2,
        New = 4,
        DataLoaded = 8
    }
}
