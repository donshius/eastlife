﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eastLIFEnet
{
    public static class Authentication
    {
        public static bool Exists(Player player)
        {
            var query = new DBQuery($"SELECT * FROM `{Player.Table}` WHERE name=@Name").AddParam("@Name", player.Name);
            var retValue = query.Execute(true).HasRows;
            query.Close();
            return retValue;
        }

        public static bool AttemptLogin(Player player, string password)
        {
            var query = new DBQuery($"SELECT * FROM `{Player.Table}` WHERE name=@Name AND password=@Password LIMIT 1");
            query.AddParam("@Name", player.Name).AddParam("@Password", Util.Hash.SHA256Hash(password)).Execute(true);
            var retValue = query.GetResult().HasRows;
            query.Close();
            return retValue;
        }

        public static void Register(Player player, string password)
        {
            DBQuery query = new DBQuery("INSERT INTO `users` (name, password) VALUES(@Name, @Password)", QueryType.INSERT_OR_DELETE);
            query.AddParam("@Name", player.Name).AddParam("@Password", Util.Hash.SHA256Hash(password)).Execute();
        }
    }
}
