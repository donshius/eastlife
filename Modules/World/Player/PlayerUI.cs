﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using eastLIFEnet.Display;
using ExtensionMethods;
using SampSharp.GameMode.Definitions;
using SampSharp.GameMode.Display;
using SampSharp.GameMode.Events;
using SampSharp.GameMode.SAMP;

namespace eastLIFEnet
{
    public static class PlayerUI
    {
        public static void GenderSelectionDialog(Player player)
        {
            var dialog = new EListDialog("Pasirinkite Lytį", "Rinktis", "Atšaukti");
            dialog.AddItem("Vyriška").AddItem("Moteriška");
            dialog.Response += (sender, e) =>
            {
                if (e.DialogButton == DialogButton.Right)
                {
                    player.SendErrorMessage("Lyties pasirinkimas yra privalomas.");
                    GenderSelectionDialog(player);
                }
                else
                {
                    player.Gender = (e.ListItem == 0) ? PlayerGender.Male : PlayerGender.Female;
                    player.SendSuccessMessage($"Pasirinkote {Color.Silver.RGB()}" + ((player.Gender == PlayerGender.Male) ? "vyrišką" : "moteriška") + $" {Color.White.RGB()}lytį.");
                }
            };
            dialog.Show(player);
        }

        public static void LoginDialog(Player player)
        {
            var dialog = new InputDialog("Prisijungimas", "Prašome prisijungti", true, "Prisijungti", "Išeiti");
            dialog.Response += (object sender, DialogResponseEventArgs e) =>
            {
                if (e.DialogButton == DialogButton.Right)
                {
                    player.Kick();
                }
                else
                {
                    if (e.InputText.Length < 6)
                    {
                        player.SendErrorMessage("Slaptažodis per trumpas, reikalingi min. 6 simboliai.");
                        LoginDialog(player);
                        return;
                    }

                    if (Authentication.AttemptLogin(player, e.InputText))
                    {
                        player.SendSuccessMessage("Sėkmingai prisijungėte.");
                        player.PState |= PlayerState.LoggedIn;
                        player.PState &= ~PlayerState.DataLoaded;
                    }
                    else
                    {
                        player.SendErrorMessage("Toks vartotojas nerastas.");
                        LoginDialog(player);
                    }
                }
            };
            dialog.Show(player);
        }

        public static void RegisterDialog(Player player)
        {
            var dialog = new InputDialog("Registracija", "Prašome užsiregistruoti", true, "Registruotis", "Išeiti");
            dialog.Response += (object sender, DialogResponseEventArgs e) =>
            {
                if (e.DialogButton == DialogButton.Right)
                {
                    player.Kick();
                }
                else
                {
                    if (e.InputText.Length < 6)
                    {
                        player.SendErrorMessage("Slaptažodis per trumpas, reikalingi min. 6 simboliai.");
                        RegisterDialog(player);
                        return;
                    }
                    Authentication.Register(player, e.InputText);
                    player.SendSuccessMessage("Užsiregistravote sėkmingai.");

                    player.PState |= PlayerState.LoggedIn;
                    player.PState |= PlayerState.New;
                    player.PState |= PlayerState.DataLoaded;
                }
            };
            dialog.Show(player);
        }
    }
}
