﻿using SampSharp.GameMode.Display;
using SampSharp.GameMode.SAMP;
using SampSharp.GameMode.World;
using SampSharp.GameMode.Events;
using SampSharp.GameMode.Definitions;
using System;
using System.Collections.Generic;
using System.Linq;
using eastLIFEnet.Commands;
using eastLIFEnet.Controllers;
using SampSharp.GameMode;
using SampSharp.GameMode.SAMP.Commands;
using eastLIFEnet.Display;
using eastLIFEnet.Modules.World.Player;
using eastLIFEnet.Util.Permissions;
using eastLIFEnet.World;
using ExtensionMethods;
using SampSharp.GameMode.SAMP.Commands.Parameters;
using SampSharp.GameMode.SAMP.Commands.ParameterTypes;

namespace eastLIFEnet
{
    public class Player : BasePlayer
    {
        #region Variables

        public static readonly string Table = "users";
        public PlayerCache Cache = new PlayerCache();
        public PlayerState PState { get; set; }
        private int money;
        public PlayerGender Gender { get; set; }
        public Player pendingTeleporter = null;
        public Player pendingTeleportTo = null;
        public List<int> JobInvites = new List<int>();
        public Job Job = null;
        public Job JobLeader = null;
        public Job JobAssistant = null;
        // Stored as a rank sqlid
        public int JobRank;

        public bool IsCuffed;

        private readonly List<Vehicle> personalVehicles = new List<Vehicle>();
        private readonly InventoryTextDraw m_InventoryTextDraw;
        #endregion

        private int _lastEntranceCheckTime = 0;

        public Player()
        {
            m_InventoryTextDraw = new InventoryTextDraw(this);
            Gender = PlayerGender.None;
            PState = PlayerState.None;
            money = 0;
            JobRank = 0;
            IsCuffed = false;
        }

        public static Player Get(string identifier)
        {
            if (int.TryParse(identifier, out int id))
                return (Player)Player.Find(id);
            else
                return (Player)Player.All.FirstOrDefault(p => p.Name.ToLower().StartsWith(identifier.ToLower()));
        }

        public override void OnCommandText(CommandTextEventArgs e)
        {
            e.Success = ECommandController.commandManager.Process(e.Text, this);
        }
        
        public override void OnConnected(EventArgs e)
        {
            if (!CheckNameFormat())
            {
                KickEx("Blogas vardo formatas. Tinkamas formatas: Vardas_Pavarde");
                return;
            }
            if (Authentication.Exists(this))
                PlayerUI.LoginDialog(this);
            else
                PlayerUI.RegisterDialog(this);

            Objects.RemoveObjectsForPlayer(this);
            base.OnConnected(e);
        }
        
        public override void OnDisconnected(DisconnectEventArgs e)
        {
            if (PState.HasFlag(PlayerState.LoggedIn) && PState.HasFlag(PlayerState.DataLoaded) && PState.HasFlag(PlayerState.Playing))
            {
                Save();
                if(personalVehicles.Count > 0)
                {
                    foreach(var veh in personalVehicles)
                    {
                        veh.Save(true);
                    }
                }
            }
            base.OnDisconnected(e);
        }
        
        public override void OnRequestSpawn(RequestSpawnEventArgs e)
        {
            if (!PState.HasFlag(PlayerState.LoggedIn)) e.PreventSpawning = true;

            base.OnRequestSpawn(e);
        }
        
        public override void OnKeyStateChanged(KeyStateChangedEventArgs e)
        {
            if(e.NewKeys.HasFlag(Keys.SecondaryAttack))
            {
                /* Limiting entrance use to 1000ms to prevent loop spam */
                if ((Server.GetTickCount() - _lastEntranceCheckTime) >= 1000)
                {
                    foreach (Entrance entrance in Entrance.entrances)
                    {
                        if (Position.DistanceTo(entrance.GetEntrancePosition()) <= 2.0f)
                        {
                            Position = entrance.GetInteriorPosition();
                            Interior = entrance.GetInteriorID();
                        }
                        else if (Position.DistanceTo(entrance.GetInteriorPosition()) <= 2.0f && Interior == entrance.GetInteriorID())
                        {
                            Position = entrance.GetEntrancePosition();
                            Interior = 0;
                        }
                    }
                    _lastEntranceCheckTime = SampSharp.GameMode.SAMP.Server.GetTickCount();
                }
            }
            base.OnKeyStateChanged(e);
        }
        
        public override void OnSpawned(SpawnEventArgs e)
        {
            m_InventoryTextDraw.CreateTextdraws();
            if (!PState.HasFlag(PlayerState.DataLoaded))
            {
                Load();
                LoadPlayerVehicles();
                PState |= PlayerState.DataLoaded;
            }
            if (PState.HasFlag(PlayerState.New))
            {
                SendInfoMessage("Jūs esate naujokas.");
                PState &= ~PlayerState.New;
            }
            if (Gender == PlayerGender.None)
            {
                PlayerUI.GenderSelectionDialog(this);
            }
            PState |= PlayerState.Playing;

            SendInfoMessage($"Jūsų state yra {(int)PState}");
            SendInfoMessage($"Jūsų darbas yra {Job.Name}");

            base.OnSpawned(e);
        }

        #region Commands

        [Command("td")]
        private void CommandTd()
        {
            m_InventoryTextDraw.Show();
            SelectTextDraw(Color.Aqua);
        }

        #region /duoti
        [Command("duoti", UsageMessage = "/duoti [žaidėjo vardas arba id] [suma]")]
        private void CommandGiveMoney([Parameter(typeof(WordType))]string giveTo, int amount)
        {
            var player = Player.Get(giveTo);
            if (player == null)
            {
                SendErrorMessage("Toks žaidėjas nerastas.");
                return;
            }
            if (amount < 1)
            {
                SendErrorMessage("Jūs negalite duoti 0 arba mažiau!");
                return;
            }
            if (amount > GetMoney())
            {
                SendErrorMessage("Jūs neturite tiek pinigų");
                return;
            }
            GiveMoney(-amount);
            player.GiveMoney(amount);
            SendCustomMessage("PINIGAI", Color.Lime.RGB(), $"Davėte {Color.Aqua.RGB()}${amount} {Color.White.RGB()}žaidėjui {Color.Aqua.RGB()}{player.Name}");
            player.SendCustomMessage("PINIGAI", Color.Lime.RGB(), $"Žaidėjas {Color.Aqua.RGB()}{Name} {Color.White.RGB()}jums davė {Color.Aqua.RGB()}${amount}");
        }
        #endregion

        #region /tpneleisti
        [Command("tpneleisti")]
        private void CommandTeleportDeny()
        {
            if (pendingTeleporter == null && pendingTeleportTo == null)
            {
                SendErrorMessage("Pas jūs niekas nenori atsiteleportuoti.");
                return;
            }
            if(pendingTeleporter != null)
                pendingTeleporter.SendInfoMessage($"Žaidėjas {Color.Aqua.RGB()}{Name} {Color.White.RGB()}nepriėme jūsų teleportacijos.");
            else
                pendingTeleportTo.SendInfoMessage($"Žaidėjas {Color.Aqua.RGB()}{Name} {Color.White.RGB()}nepriėme jūsų teleportacijos.");

            pendingTeleporter = null;
            pendingTeleportTo = null;
            SendSuccessMessage("Teleportacija uždrausta.");
        }
        #endregion

        #region /tpleisti
        [Command("tpleisti")]
        private void CommandTeleportAllow()
        {
            if(pendingTeleporter == null && pendingTeleportTo == null)
            {
                SendErrorMessage("Pas jūs niekas nenori atsiteleportuoti.");
                return;
            }
            if(pendingTeleporter != null)
            {
                pendingTeleporter.Position = Position;
                pendingTeleporter.SendInfoMessage($"Žaidėjas {Color.Aqua.RGB()}{Name} {Color.White.RGB()}priėme jūsų teleportacija.");
                pendingTeleporter = null;
            }
            else
            {
                Position = pendingTeleportTo.Position;
                pendingTeleportTo.SendInfoMessage($"Žaidėjas {Color.Aqua.RGB()}{Name} {Color.White.RGB()}priėme jūsų teleportacija.");
                pendingTeleportTo = null;
            }
            SendSuccessMessage("Teleportacija leista.");
        }
        #endregion

        #region /to
        [Command("to", PermissionChecker = typeof(AdminPermission), UsageMessage = "/to [žaidėjo vardas arba id]")]
        private void CommandTeleportTo([Parameter(typeof(WordType))]string teleportTo)
        {
            var player = Player.Get(teleportTo);
            if(player == null)
            {
                SendErrorMessage("Toks žaidėjas nerastas.");
                return;
            }
            player.SendCustomMessage("TELEPORT", Color.Aqua.RGB(), $"Žaidėjas {Color.Silver.RGB()}{Name} {Color.White.RGB()}nori atsiteleportuoti pas jus.");
            player.SendCustomMessage("TELEPORT", Color.Aqua.RGB(), $"Jeigu norite leisti teleportacija naudokite {Color.LightGreen.RGB()}/tpleisti {Color.White.RGB()}jeigu ne - {Color.Red.RGB()}/tpneleisti");
            player.pendingTeleportTo = null;
            player.pendingTeleporter = this;
            SendCustomMessage("TELEPORT", Color.Aqua.RGB(), "Teleportacijos prašymas išsiųstas.");
        }
        #endregion

        #region /get
        [Command("get", PermissionChecker = typeof(AdminPermission), UsageMessage = "/get [žaidėjo vardas arba id]")]
        private void CommandTeleportGet([Parameter(typeof(WordType))]string teleportWho)
        {
            var player = Player.Get(teleportWho);
            if(player == null)
            {
                SendErrorMessage("Toks žaidėjas nerastas.");
                return;
            }
            player.SendCustomMessage("TELEPORT", Color.Aqua.RGB(), $"Žaidėjas {Color.Silver.RGB()}{Name} {Color.White.RGB()}nori nuteleportuoti jus pas save.");
            player.SendCustomMessage("TELEPORT", Color.Aqua.RGB(), $"Jeigu norite leisti teleportacija naudokite {Color.LightGreen.RGB()}/tpleisti {Color.White.RGB()}jeigu ne - {Color.Red.RGB()}/tpneleisti");
            player.pendingTeleporter = null;
            player.pendingTeleportTo = this;
            SendCustomMessage("TELEPORT", Color.Aqua.RGB(), "Teleportacijos prašymas išsiųstas.");
        }
        #endregion

        #endregion

        #region Money
        public override void GiveMoney(int money)
        {
            this.money += money;
            base.GiveMoney(money);
        }

        public int GetMoney()
        {
            return money;
        }

        public void SetMoney(int money)
        {
            this.money = money;
            Money = money;
        }

        public void CheckMoney()
        {
            if (Money != money) Money = money;
        }
        #endregion

        public bool CheckNameFormat()
        {
            if (Name.IndexOf("_") == -1) return false;
            if (Name.Count(s => s == '_') > 1) return false;
            string[] nameParts = Name.Split('_');
            if (nameParts[0].Length < 3 || nameParts[1].Length < 3) return false;
            nameParts[0] = nameParts[0].ToLower();
            nameParts[0] = nameParts[0].First().ToString().ToUpper() + string.Join("", nameParts[0].Skip(1));
            nameParts[1] = nameParts[1].ToLower();
            nameParts[1] = nameParts[1].First().ToString().ToUpper() + string.Join("", nameParts[1].Skip(1));
            var correctName = nameParts[0] + "_" + nameParts[1];
            return correctName.Equals(Name);
        }

        public void KickEx(string reason, Player administrator = null)
        {
            SendCustomMessage("KICK", Util.Color.HEX_ORANGE, "Jūs buvote išmestas iš serverio.");
            SendCustomMessage("KICK", Util.Color.HEX_ORANGE, $"Laikas: {Color.Silver.RGB()}{DateTime.Now:yyyy-MM-dd HH:mm:ss}");
            SendCustomMessage("KICK", Util.Color.HEX_ORANGE, $"Priežastis: {Color.Lime.RGB()}{reason}");
            if(administrator != null) SendCustomMessage("KICK", Util.Color.HEX_ORANGE, $"Administratorius: {Color.Navy.RGB()}{administrator.Name}");

            var timer = new Timer(200, false);
            timer.Tick += (object sender, EventArgs e) =>
            {
                Kick();
            };
        }
        
        public void SendInfoMessage(string message)
        {
            SendClientMessage($"{Color.Orange.RGB()}Informacija {Color.Silver.RGB()}» {Color.White.RGB()}{message}");
        }

        public void SendCustomMessage(string title, string color, string message)
        {
            SendClientMessage($"{color}{title} {Color.Silver.RGB()}» {Color.White.RGB()}{message}");
        }

        public void SendSuccessMessage(string message)
        {
            SendClientMessage($"{Color.Green.RGB()}Atlikta {Color.Silver.RGB()}» {Color.White.RGB()}{message}");
        }

        public void SendErrorMessage(string message)
        {
            SendClientMessage($"{Color.Red.Lighten(0.16f).RGB()}Klaida {Color.Silver.RGB()}» {Color.White.RGB()}{message}");
        }

        public void SendUsageMessage(string message)
        {
            SendCustomMessage("Komanda", Color.Orange.RGB(), $"Naudojimas: {Color.Silver.RGB()}{message}");
        }

        #region Database methods

        private void Load()
        {
            Console.WriteLine("> Loading data for player " + Name);
            var query = new DBQuery("SELECT * FROM `users` WHERE name=@Name");
            query.AddParam("@Name", Name).Execute(true);

            Position = new SampSharp.GameMode.Vector3(query.GetResult().GetFloat("posX"), query.GetResult().GetFloat("posY"), query.GetResult().GetFloat("posZ"));
            GiveMoney(query.GetResult().GetInt32("money"));
            Gender = (PlayerGender)query.GetResult().GetInt32("gender");
            Job = Job.Find(query.GetResult().GetInt32("job"));
            JobLeader = Job.Find(query.GetResult().GetInt32("jobLeader"));
            JobRank = query.GetResult().GetInt32("jobRank");
            JobAssistant = Job.Find(query.GetResult().GetInt32("jobAssistant"));

            var jobInviteStr = query.GetResult().GetString("jobInvites");
            if (jobInviteStr.Length > 0)
            {
                var jobInviteIds = jobInviteStr.Split('|');
                foreach (var jobInviteId in jobInviteIds)
                {
                    if (int.TryParse(jobInviteId, out int parsedJobId))
                    {
                        if(!JobInvites.Contains(parsedJobId)) JobInvites.Add(parsedJobId);
                    }
                }
            }

            query.Close();
        }

        private void Save()
        {
            var query = new DBQuery("UPDATE `users` SET money=@Money, posX=@posX, posY=@posY, posZ=@posZ WHERE name=@Name", QueryType.INSERT_OR_DELETE);
            query.AddParam("@posX", Position.X).AddParam("@posY", Position.Y).AddParam("@posZ", Position.Z);
            query.AddParam("@Money", GetMoney()).AddParam("@Name", Name);
            query.Execute();

            var jobInviteStr = "";
            foreach (var jobInviteId in JobInvites)
            {
                if (jobInviteStr.Length != 0) jobInviteStr += "|";
                jobInviteStr += jobInviteId;
            }

            query = new DBQuery($"UPDATE `{Table}` SET jobInvites=@jobInvites, jobRank=@jobRank, jobLeader=@jobLeader, jobAssistant=@jobAssistant WHERE name=@Name", QueryType.INSERT_OR_DELETE);
            query.AddParam("@jobInvites", jobInviteStr).AddParam("@Name", Name);
            query.AddParam("@jobRank", JobRank);
            query.AddParam("@jobLeader", JobLeader?.ID ?? 0);
            query.AddParam("@jobAssistant", JobAssistant?.ID ?? 0);
            query.Execute();
        }

        private void LoadPlayerVehicles()
        {
            Console.WriteLine("> Loading vehicles for player " + Name);
            var query = new DBQuery("SELECT * FROM `vehicles` WHERE owner=@Owner").AddParam("@Owner", Name);
            query.Execute(true);

            if(query.GetResult().HasRows)
            {
                Console.WriteLine("> Player has cars...");
                Vehicle vehicle = (Vehicle)BaseVehicle.Create((VehicleModelType)query.GetResult().GetInt32("model"), new Vector3(query.GetResult().GetFloat("posX"), query.GetResult().GetFloat("posY"), query.GetResult().GetFloat("posZ")),
                query.GetResult().GetFloat("posA"), query.GetResult().GetInt32("color1"), query.GetResult().GetInt32("color2"), -1);

                vehicle.SetSQLID(query.GetResult().GetInt32("id"));
                vehicle.SetOwner(this);

                personalVehicles.Add(vehicle);
            }
            else
            {
                Console.WriteLine("> Player doesn't have cars...");
            }

            query.Close();
        }
        #endregion
    }
}
