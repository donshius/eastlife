﻿using SampSharp.GameMode.World;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eastLIFEnet
{
    class Vehicle : BaseVehicle
    {
        #region Variables
        private Player m_Owner = null;
        private int m_SQLID = -1;
        public Job Job = null;
        #endregion

        public Player GetOwner()
        {
            return m_Owner;
        }

        public void SetOwner(Player owner)
        {
            m_Owner = owner;
        }

        public bool IsJobVehicle()
        {
            return Job != null;
        }

        public void SetSQLID(int val)
        {
            m_SQLID = val;
        }

        public void Save(bool removeFromWorld=false)
        {
            if(m_SQLID > 0)
            {
                if(m_Owner != null && m_Owner.IsConnected == true)
                {
                    DBQuery query = new DBQuery("UPDATE `vehicles` SET posX=@posX, posY=@posY, posZ=@posZ, posA=@posA, owner=@Owner WHERE id=@ID", QueryType.INSERT_OR_DELETE);
                    query.AddParam("@posX", Position.X).AddParam("@posY", Position.Y).AddParam("@posZ", Position.Z).AddParam("@posA", Angle);
                    query.AddParam("@Owner", m_Owner.Name);
                    query.AddParam("@ID", m_SQLID);

                    query.Execute();

                    if (removeFromWorld)
                        this.Dispose();
                }
            }
        }
    }
}
