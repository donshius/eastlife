﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eastLIFEnet
{
    public class BusinessResource
    {
        public static List<BusinessResource> businessResources = new List<BusinessResource>();
        private static int idCount = -1;

        private string m_Name;
        private int m_MaxQuantity;
        private int m_ID;
        private string m_Unit;

        public BusinessResource(string name, int maxQuantity, string unit)
        {
            m_Name = name;
            m_MaxQuantity = maxQuantity;
            m_ID = ++idCount;
            m_Unit = unit;
        }

        public static BusinessResource Find(int id)
        {
            return businessResources.FirstOrDefault(r => r.GetID() == id);
        }

        public string GetName()
        {
            return m_Name;
        }

        public int GetID()
        {
            return m_ID;
        }

        public int GetMaxQuantity()
        {
            return m_MaxQuantity;
        }

        public string GetUnit()
        {
            return m_Unit;
        }

        public static void Create(string name, int maxQuantity, string unit)
        {
            BusinessResource businessResource = new BusinessResource(name, maxQuantity, unit);
            businessResources.Add(businessResource);
        }

        public static void Init()
        {
            Create("Kavos pupeles", 1000, "vnt");
            Create("Benzinas", 2000, "L");
            Create("Dujos", 10000, "L");
            Create("Dyzelis", 2000, "L");
        }
    }
}
