﻿using System;
using System.Collections.Generic;
using ExtensionMethods;
using SampSharp.GameMode;
using SampSharp.GameMode.SAMP;
using SampSharp.GameMode.World;

namespace eastLIFEnet
{
    public class Business
    {
        public static List<Business> businesses = new List<Business>();

        public int Sqlid;
        public string Name { get; }
        public string OwnerName { get; set; }
        private Vector3 Position { get; }
        public int ProfitPerHour { get; }
        public int CurrentProfit { get; set; }
        public int Price { get; }

        private List<Tuple<BusinessResource, int>> businessResources = new List<Tuple<BusinessResource, int>>();

        private TextLabel m_Label = null;
        private Pickup m_Pickup = null;

        public Business(string name, string ownerName, Vector3 position, int SQLID, int profitPerHour, int currentProfit, int price)
        {
            Name = name;
            OwnerName = ownerName;
            Position = position;
            Sqlid = SQLID;
            ProfitPerHour = profitPerHour;
            CurrentProfit = currentProfit;
            Price = price;

            UpdateWorldObjects();
        }

        public static bool CMD_biznis(Player player, string ps)
        {
            var business = Business.FindNereastBusiness(player);
            if(business == null || business.Position.DistanceTo(player.Position) > 2.5f)
            {
                player.SendErrorMessage("Jūs neesate prie biznio!");
                return true;
            }

            if (!business.HasOwner())
            {
                BusinessUI.BuyDialog(player, business);
            }

            if(!business.OwnerName.Equals(player.Name))
            {
                player.SendErrorMessage("Jūs neesate šio biznio savininkas.");
                return true;
            }

            BusinessUI.ControlDialog(player, business);
            return true;
        }

        public void UpdateWorldObjects()
        {
            m_Label?.Dispose();

            m_Label?.Dispose();

            if(HasOwner())
            {
                m_Label = new TextLabel(
                    $"Biznis [ {Color.Orange.RGB()}{Name} {Color.White.RGB()}]\n\n"
                  + $"Savininkas: {Color.Teal.RGB()}{OwnerName}\n"
                  + $"{Color.White.RGB()}Uždarbis: {Color.Teal.RGB()}{ProfitPerHour}/val.\n"
                  + $"{Color.White.RGB()}Valdymas - {Color.LightGreen.RGB()}/biznis",
                Color.White, Position, 7.5f, 0, true);
            }
            else
            {

                m_Label = new TextLabel(
                    $"Biznis [ {Color.Orange.RGB()}{Name} {Color.White.RGB()}]\n\n"
                  + $"Uždarbis: {Color.Teal.RGB()}{ProfitPerHour}/val.\n"
                  + $"{Color.Orange.RGB()}Biznis yra parduodamas!\n"
                  + $"{Color.White.RGB()}Kaina: {Color.LightGreen.RGB()}${Price}\n"
                  + $"{Color.White.RGB()}Norėdami pirkti - rašykite {Color.LightGreen.RGB()}/biznis",
                Color.White, Position, 7.5f, 0, true);
            }


            m_Pickup = Pickup.Create(1274, 1, Position);
        }

        public void Save()
        {
            string resourceString = "";
            foreach(Tuple<BusinessResource, int> res in businessResources)
            {
                if (resourceString.Length != 0) resourceString += "|";
                resourceString += res.Item1.GetID() + ":" + res.Item2;
            }

            DBQuery query = new DBQuery(string.Format("UPDATE `businesses` SET currentProfit='{0}', owner='{1}', resources='{2}' WHERE id='{3}'", CurrentProfit, OwnerName, resourceString, Sqlid), QueryType.INSERT_OR_DELETE);
            query.Execute();
        }

        public bool HasOwner()
        {
            return !OwnerName.Equals("nera");
        }

        public Player GetOwner()
        {
            return Player.Get(OwnerName);
        }

        public List<Tuple<BusinessResource, int>> GetBusinessResources()
        {
            return businessResources;
        }

        public static Business FindNereastBusiness(Player player)
        {
            Business nearestBusiness = null;
            foreach(Business business in businesses)
            {
                if (nearestBusiness == null)
                {
                    nearestBusiness = business;
                    continue;
                }

                if (business.Position.DistanceTo(player.Position) < nearestBusiness.Position.DistanceTo(player.Position))
                    nearestBusiness = business;
            }
            return nearestBusiness;
        }

        public static void SaveAll()
        {
            Console.WriteLine("> Saving all businesses...");
            foreach(Business business in businesses)
                business.Save();
        }

        public static void LoadAll()
        {
            Console.WriteLine("> Loading businesses...");
            Console.WriteLine("> Found {0} businesses...", DBQuery.GetNumberRowsInTable("businesses"));

            DBQuery query = new DBQuery("SELECT * FROM `businesses`");
            query.Execute();

            if (query.GetResult().HasRows)
            {
                while (query.GetResult().Read())
                {
                    Vector3 position = new Vector3(query.GetResult().GetFloat("posX"), query.GetResult().GetFloat("posY"), query.GetResult().GetFloat("posZ"));
                    Business business = new Business(query.GetResult().GetString("name"), query.GetResult().GetString("owner"), position, query.GetResult().GetInt32("id"),
                        query.GetResult().GetInt32("profitPerHour"), query.GetResult().GetInt32("currentProfit"), query.GetResult().GetInt32("price"));

                    /*
                     * How resources are loaded:
                     * resourceid:quantity|resourceid:quantity
                     */
                    string[] resourceStrings = query.GetResult().GetString("resources").Split('|');

                    foreach(string resourceString in resourceStrings)
                    {
                        string[] resourceIdAndQuant = resourceString.Split(':');
                        business.businessResources.Add(new Tuple<BusinessResource, int>(BusinessResource.Find(Convert.ToInt32(resourceIdAndQuant[0])), Convert.ToInt32(resourceIdAndQuant[1])));
                    }

                    businesses.Add(business);
                }
            }

            query.Close();
        }
    }
}
