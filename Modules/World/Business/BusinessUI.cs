﻿using System;
using eastLIFEnet.Display;
using ExtensionMethods;
using SampSharp.GameMode.Definitions;
using SampSharp.GameMode.Display;
using SampSharp.GameMode.Events;
using SampSharp.GameMode.SAMP;

namespace eastLIFEnet
{
    public static class BusinessUI
    {
        public static void BuyDialog(Player player, Business business)
        {
            var dialog = new MessageDialog($"Biznio '{business.Name}' pirkimas",
                $"{Color.White.RGB()}Šis biznis yra parduodamas, norėdami ji pirkti spauskite {Color.Lime.RGB()}'Pirkti'\n\n"
                + $"{Color.White.RGB()}Biznio pavadinimas: {Color.Cyan.RGB()}{business.Name}\n"
                + $"{Color.White.RGB()}Biznio kaina: {Color.Cyan.RGB()}${business.Price}\n"
                + $"{Color.White.RGB()}Biznio uždarbis per valandą: {Color.Cyan.RGB()}${business.ProfitPerHour}", "Pirkti", "Atšaukti");

            dialog.Response += (object sender, DialogResponseEventArgs e) =>
            {
                if(player.GetMoney() < business.Price)
                {
                    player.SendErrorMessage("Jūs neturite pakankamai pinigų.");
                    return;
                }
                player.GiveMoney(-business.Price);
                business.OwnerName = player.Name;
                business.UpdateWorldObjects();
                business.Save();
                player.SendSuccessMessage("Biznis sėkmingai nupirktas!");
            };

            dialog.Show(player);
        }

        public static void ControlDialog(Player player, Business business)
        {
            var dialog = new EListDialog($"Biznio '{business.Name}' valdymas", "Rinktis", "Išeiti");
            dialog.AddItem("Informacija");
            dialog.AddItem("Uždarbis");
            dialog.AddItem("Resursai");
            dialog.AddItem("Pardavimas");

            dialog.Response += (sender, e) =>
            {
                switch (e.ListItem)
                {
                    case 0:
                        ControlInfoDialog(player, business);
                        break;
                    case 1:
                        ControlProfitDialog(player, business);
                        break;
                    case 2:
                        ControlResourceDialog(player, business);
                        break;
                    case 3:
                        // TODO: pardavimas
                        break;
                    default:
                        break;
                }
            };

            dialog.Show(player);
        }

        private static void ControlInfoDialog(Player player, Business business)
        {
            int currentResources = 0, maxResources = 0;
            foreach(var businessRes in business.GetBusinessResources())
            {
                currentResources += businessRes.Item2;
                maxResources += businessRes.Item1.GetMaxQuantity();
            }
            var dialog = new MessageDialog($"Biznio '{business.Name}' informacija",
                $"{Color.White.RGB()}Pavadinimas: {Color.Aqua.RGB()}{business.Name}\n"
                + $"{Color.White.RGB()}Uždarbis per valandą: {Color.Aqua.RGB()}${business.ProfitPerHour}\n"
                + $"{Color.White.RGB()}Dabartinis uždarbis: {Color.Aqua.RGB()}${business.CurrentProfit}\n"
                + $"{Color.White.RGB()}Resursai: {Color.Aqua.RGB()}{currentResources}{Color.White.RGB()}/{Color.Aqua.RGB()}{maxResources}",
                "Gerai"
            );
            dialog.Show(player);
        }

        private static void ControlProfitDialog(Player player, Business business)
        {
            var dialog = new MessageDialog($"Biznio '{business.Name}' uždarbis",
                $"{Color.White.RGB()}Jūsų biznis šiuo metu yra uždirbes {Color.Aqua.RGB()}${business.CurrentProfit}.\n\n"
                + $"{Color.White.RGB()}Jeigu norite nuimti šį pelną, spauskite {Color.Lime.RGB()}'Nuimti'", "Nuimti", "Išeiti");
            dialog.Response += (object sender2, DialogResponseEventArgs e2) =>
            {
                if (e2.DialogButton != DialogButton.Left) return;

                ControlProfitWithdrawDialog(player, business);
            };
            dialog.Show(player);
        }

        private static void ControlProfitWithdrawDialog(Player player, Business business)
        {
            var dialog = new InputDialog($"Biznio '{business.Name}' pelno nuėmimas",
                $"{Color.White.RGB()}Šis biznis turi {Color.Aqua.RGB()}${business.CurrentProfit} {Color.White.RGB()}pelno.\n"
              + $"{Color.White.RGB()}Įveskite sumą kuria norite nuimti:"
                , false, "Nuimti", "Išeiti");
            dialog.Response += (object sender3, DialogResponseEventArgs e3) =>
            {
                if (e3.DialogButton != DialogButton.Left) return;
                if (!int.TryParse(e3.InputText, out int profitWithdrawAmount))
                {
                    player.SendErrorMessage("Įvesta suma turi būti skaičius!");
                    return;
                }
                if(profitWithdrawAmount < business.CurrentProfit)
                {
                    player.SendErrorMessage("Jūsų biznis neturi tiek pelno!");
                    return;
                }
                player.GiveMoney(profitWithdrawAmount);
                business.CurrentProfit -= profitWithdrawAmount;
            };
            dialog.Show(player);
        }

        private static void ControlResourceDialog(Player player, Business business)
        {
            var dialog = new TablistDialog("Resursai", new string[] {"#", "Pavadinimas", "Kiekis", "Max kiekis"}, "Rinktis", "Išeiti");

            dialog.Response += (object sender2, DialogResponseEventArgs e2) =>
            {
                var foundResource = business.GetBusinessResources()[e2.ListItem].Item1;
                var resourceDialog = new EListDialog($"'{foundResource.GetName()}' resursas", "Rinktis", "Išeiti");
                resourceDialog.AddItem("Užsakyti");
                resourceDialog.Show(player);
            };

            var count = 1;
            foreach(var res in business.GetBusinessResources())
            {
                dialog.Add(count++ + ".", res.Item1.GetName(), res.Item2.ToString() + res.Item1.GetUnit(), res.Item1.GetMaxQuantity().ToString() + res.Item1.GetUnit());
            }
            dialog.Show(player);
        }
    }
}