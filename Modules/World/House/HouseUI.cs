﻿using System.Linq;
using eastLIFEnet.Display;
using eastLIFEnet.World;
using ExtensionMethods;
using SampSharp.GameMode.Definitions;
using SampSharp.GameMode.Display;
using SampSharp.GameMode.SAMP;

namespace eastLIFEnet
{
    public static class HouseUI
    {
        public static void BuyDialog(Player player, House house)
        {
            var mDialog = new MessageDialog("Namo pirkimas",
                    $"{Color.White.RGB()}Šis namas yra parduodamas!\nNorėdami pirkti šį namą, spauskite {Color.Lime.RGB()}'Pirkti'\n\n"
                  + $"{Color.White.RGB()}Namo kaina: {Color.Cyan.RGB()}${house.Price}\n"
                  + $"{Color.White.RGB()}Interjeras: {Color.Cyan.RGB()}{House.GetInteriorDescription(house.InteriorIndex)}", "Pirkti", "Išeiti");

            mDialog.Response += (sender, e) =>
            {
                if (e.DialogButton != DialogButton.Left) return;
                if (House.GetOwnedHouses(player).Count == House.MAX_HOUSES_PER_PLAYER)
                {
                    player.SendErrorMessage($"Jūs galite turėti tik ${House.MAX_HOUSES_PER_PLAYER} namą(-ų).");
                    return;
                }
                if (player.GetMoney() < house.Price)
                {
                    player.SendErrorMessage("Jūs neturite pakankamai pinigų.");
                    return;
                }
                player.GiveMoney(-house.Price);
                house.OwnerName = player.Name;
                house.UpdateWorldObjects();
                house.Save();
                player.SendSuccessMessage("Namas sėkmingai nupirktas!");
            };

            mDialog.Show(player);
        }

        public static void ControlDialog(Player player, House house)
        {
            var dialog = new EListDialog("Namo Valdymas", "Rinktis", "Išeiti");
            dialog.AddItem("Namo informacija");

            /* Easier flow to understand */
            if (house.HasTenant())
            {
                dialog.AddItem("Atšaukti nuomą");
            }
            else
            {
                dialog.AddItem("Užrakto valdymas");
                dialog.AddItem(house.PlacedForRent ? "Atšaukti nuomą" : "Išnomuoti namą");
                dialog.AddItem("Namo seifas");
                dialog.AddItem("Namo pardavimas");
            }

            dialog.Response += (sender, e) =>
            {
                if (e.DialogButton != DialogButton.Left) return;
                if (e.ListItem == 0) // Namo info
                {
                    ControlInfoDialog(player, house);
                }
                else if (e.ListItem == 1 && !house.HasTenant()) // uzraktas
                {
                    ControlLockDialog(player, house);
                }
                else if (e.ListItem == 2 && !house.HasTenant()) // isnomuoti
                {
                    if (house.PlacedForRent)
                    {
                        ControlRentCancelPlacing(player, house);
                    }
                    else
                    {
                        ControlRentPlace(player, house);
                    }
                }
                else if (e.ListItem == 1 && house.HasTenant())
                {
                    ControlRentCancelFuturePlacing(player, house);
                }
                else if (e.ListItem == 3 && !house.HasTenant()) // Seifas
                {
                    ControlSafe(player, house);
                }
                else if (e.ListItem == 4 && !house.HasTenant())
                {
                    ControlSell(player, house);
                }
            };
            dialog.Show(player);
        }

        private static void ControlInfoDialog(Player player, House house)
        {
            var message = $"{Color.White.RGB()}Namo vertė: {Color.Aqua.RGB()}${house.Price}\n"
                                  + $"{Color.White.RGB()}Pinigai seife: {Color.Aqua.RGB()}${house.OwnerSafe}\n";
            if (house.HasTenant())
            {
                message += $"\n{Color.White.RGB()}Išnomuotas iki: {Color.Aqua.RGB()}{Util.Date.FromUnix(house.RentedUntil):HH:mm:ss yyyy-MM-dd}\n";
                message += $"{Color.White.RGB()}Nuomininkas: {Color.Aqua.RGB()}{house.TenantName}\n";
            }

            var dialog = new MessageDialog("Namo Informacija", message, "Gerai");

            dialog.Show(player);
        }

        private static void ControlLockDialog(Player player, House house)
        {
            var dialog = new EListDialog("Užrakto valdymas", "Rinktis", "Išeiti");
            dialog.AddItem(house.Lock
                ? $"Užraktas: {Color.Green.RGB()}Užrakintas"
                : $"Užraktas: {Color.Red.RGB()}Atrakintas");
            dialog.AddItem(house.Lock
                ? "Atrakinti"
                : "Užrakinti");

            dialog.Response += (sender2, e2) =>
            {
                if (e2.DialogButton != DialogButton.Left)
                {
                    ControlDialog(player, house);
                    return;
                }
                if (e2.ListItem != 1) return;

                house.Lock = !house.Lock;

                player.SendSuccessMessage(house.Lock ? "Namas užrakintas." : "Namas atrakintas.");

                house.Save();
                ControlLockDialog(player, house);
            };

            dialog.Show(player);
        }

        private static void ControlRentCancelPlacing(Player player, House house)
        {
            var dialog = new MessageDialog("Nuomos atšaukimas",
                        $"{Color.White.RGB()}Šis namas yra paduotas nuomai.\n" +
                        $"Norėdami tai atšaukti spauskite {Color.Aqua.RGB()}'Atšaukti'", "Atšaukti", "Išeiti");

            dialog.Response += (sender2, e2) =>
            {
                if (e2.DialogButton != DialogButton.Left) return;

                house.PlacedForRent = false;
                house.Save();
                house.UpdateWorldObjects();

                player.SendSuccessMessage("Nuoma atšaukta.");
            };

            dialog.Show(player);
        }

        private static void ControlRentPlace(Player player, House house)
        {
            var dialog = new InputDialog("Namo nuomos terminas",
                            $"{Color.White.RGB()}Kaip šio namo savininkas, jūs turite galimybę išnomuoti šį namą kitiems žaidėjams.\n" +
                            $"{Color.White.RGB()}Minimalus nuomos terminas yra {Color.Aqua.RGB()}1 diena{Color.White.RGB()}, maksimalus - {Color.Aqua.RGB()}14 dienų\n" +
                            $"{Color.White.RGB()}Įrašykite kiek dienų norite išnomuoti namą:",
                            false, "Toliau", "Išeiti");

            dialog.Response += (sender2, e2) =>
            {
                if (e2.DialogButton != DialogButton.Left) return;
                if (!int.TryParse(e2.InputText, out int rentDuration))
                {
                    player.SendErrorMessage("Nuomos terminas turi būti skaičius!");
                    return;
                }
                if (rentDuration < 1 || rentDuration > 14)
                {
                    player.SendErrorMessage("Nuomos terminas turi būti tarp 1 ir 14 dienų!");
                    return;
                }
                ControlRentPlacePrice(player, house, rentDuration);
            };

            dialog.Show(player);
        }

        private static void ControlRentPlacePrice(Player player, House house, int rentDuration)
        {
            var dialog = new InputDialog("Namo nuomos kaina",
                    $"{Color.White.RGB()}Jūs norite išnomuoti šį namą {Color.Aqua.RGB()}{rentDuration} {Color.White.RGB()}dienų(-ai).\n" +
                    $"{Color.White.RGB()}Nustatykite šios nuomos kainą už visą terminą:",
                    false, "Toliau", "Išeiti");

            dialog.Response += (sender3, e3) =>
            {
                if (e3.DialogButton != DialogButton.Left) return;
                if (!int.TryParse(e3.InputText, out int rentPrice))
                {
                    player.SendErrorMessage("Nuomos kainą turi būti skaičius!");
                    return;
                }
                if (rentPrice < 1 || rentPrice > 100000)
                {
                    player.SendErrorMessage("Nuomos kainą turi būti tarp $1 ir $100,000!");
                    return;
                }
                house.RentDuration = rentDuration;
                house.RentPrice = rentPrice;
                house.PlacedForRent = true;
                house.UpdateWorldObjects();
                house.Save();
                player.SendSuccessMessage("Namas paduotas nuomai. Jūs galite pilnai naudotis namu iki kol atsiras nuomininkas.");
            };

            dialog.Show(player);
        }

        private static void ControlRentCancelFuturePlacing(Player player, House house)
        {
            var dialog = new MessageDialog("Nuomos Atšaukimas",
                        $"{Color.White.RGB()}Šis namas jau yra išnomuotas ir turi nuominką.\n" +
                        $"{Color.White.RGB()}Atšaukus nuomą, namas nebus išnomuojamas pasibaigus šiam terminui.\n" +
                        $"{Color.White.RGB()}Namo pilnai negalėsite naudotis iki kol šis nuomos terminas baigsis",
                    "Atšaukti", "Išeiti");

            dialog.Response += (sender2, e2) =>
            {
                if (e2.DialogButton != DialogButton.Left) return;

                house.PlacedForRent = false;
                house.UpdateWorldObjects();
                house.Save();

                player.SendSuccessMessage("Nuoma atšaukta!");
            };

            dialog.Show(player);
        }

        private static void ControlSafe(Player player, House house)
        {
            var dialog = new EListDialog("Namo Seifas", "Rinktis", "Išeiti");
            dialog.AddItem($"Turima suma: {Color.Aqua.RGB()}${house.OwnerSafe}");
            dialog.AddItem("Įdėti pinigų");
            dialog.AddItem("Išimti pinigų");

            dialog.Response += (sender2, e2) =>
            {
                if (e2.DialogButton != DialogButton.Left) return;
                if (e2.ListItem == 1)
                {
                    ControlSafeDeposit(player, house);
                }
                else if (e2.ListItem == 2)
                {
                    ControlSafeWithdraw(player, house);
                }
            };
            dialog.Show(player);
        }

        private static void ControlSafeDeposit(Player player, House house)
        {
            var dialog = new InputDialog("Įdėti pinigų",
                        $"{Color.White.RGB()}Įrašykite kiek pinigų norite įdėti į seifą:",
                    false, "Įdėti", "Išeiti");

            dialog.Response += (sender3, e3) =>
            {
                if (!int.TryParse(e3.InputText, out int safeAddAmount))
                {
                    player.SendErrorMessage("Suma turi būti skaičius.");
                    return;
                }
                if (safeAddAmount < 1)
                {
                    player.SendErrorMessage("Suma turi būti daugiau negu 0!");
                    return;
                }
                if (safeAddAmount > player.GetMoney())
                {
                    player.SendErrorMessage("Jūs tiek pinigų neturite!");
                    return;
                }
                house.OwnerSafe += safeAddAmount;
                player.GiveMoney(-safeAddAmount);
                player.SendSuccessMessage($"Pridėjote {Color.Aqua.RGB()}${safeAddAmount} {Color.White.RGB()}pinigų į seifą.");
            };

            dialog.Show(player);
        }

        private static void ControlSafeWithdraw(Player player, House house)
        {
            var dialog = new InputDialog("Išimti pinigų",
                        $"{Color.White.RGB()}Jūs seife turite: {Color.Aqua.RGB()}${house.OwnerSafe}" +
                        $"{Color.White.RGB()}Įrašykite kiek pinigų norite išimti iš seifo:",
                    false, "Išimti", "Išeiti");

            dialog.Response += (sender3, e3) =>
            {
                if (!int.TryParse(e3.InputText, out int safeTakeAmount))
                {
                    player.SendErrorMessage("Pinigų kiekis turi būti skaičius!");
                    return;
                }
                if (safeTakeAmount < 1)
                {
                    player.SendErrorMessage("Pinigų kiekis negali būti mažiau negu 1");
                    return;
                }
                if (safeTakeAmount > house.OwnerSafe)
                {
                    player.SendErrorMessage("Tiek pingių seife nėra!");
                    return;
                }
                house.OwnerSafe -= safeTakeAmount;
                player.GiveMoney(safeTakeAmount);
                player.SendSuccessMessage($"Sėkmingai išsiėmete {Color.Aqua.RGB()}${safeTakeAmount}");
            };

            dialog.Show(player);
        }

        private static void ControlSell(Player player, House house)
        {
            var dialog = new EListDialog("Namo Pardavimas", "Rinktis", "Išeiti");
            dialog.AddItem("Parduoti namą serveriui");
            dialog.AddItem("Parduoti namą žaidejui");

            dialog.Response += (sender2, e2) =>
            {
                if (e2.DialogButton != DialogButton.Left) return;

                if (e2.ListItem == 0)
                {
                    ControlSellServer(player, house);
                }
                else if (e2.ListItem == 1)
                {
                    ControlSellPlayer(player, house);
                }
            };

            dialog.Show(player);
        }

        private static void ControlSellServer(Player player, House house)
        {
            var dialog = new MessageDialog("Namo pardavimas serveriui",
                        $"{Color.White.RGB()}Jūs turite galimybe parduoti namą serveriui\n" +
                        $"Parduodami namą serveriui jūs gausite tik pusę namo vertės.\n" +
                        $"Šiuo atvejų jus gautumete: {Color.Aqua.RGB()}${house.Price / 2}\n\n" +
                        $"{Color.White.RGB()}Ar tikrai norite parduoti namą serveriui?",
            "Parduoti", "Išeiti");

            dialog.Response += (sender3, e3) =>
            {
                if (e3.DialogButton != DialogButton.Left) return;

                if (house.OwnerSafe > 0) player.GiveMoney(house.OwnerSafe);

                player.GiveMoney(house.Price / 2);

                house.Clear(true);
                house.UpdateWorldObjects();

                player.SendSuccessMessage("Namas sėkmingai parduotas.");
            };

            dialog.Show(player);
        }

        private static void ControlSellPlayer(Player player, House house)
        {
            var dialog = new MessageDialog("Namo pardavimas žaidėjui",
                        $"{Color.White.RGB()}Jūs turite galimybe parduoti namą žaidėjui\n" +
                        $"Įveskite žaidėjo vardą kuriam norite parduoti namą:",
            "Parduoti", "Išeiti");

            dialog.Response += (sender3, e3) =>
            {
                if (e3.DialogButton != DialogButton.Left) return;
            };

            dialog.Show(player);
        }

        public static void RentDialog(Player player, House house)
        {
            var dialog = new MessageDialog("Namo nuoma",
                    $"{Color.White.RGB()}Šis namas yra nuomojamas.\n" +
                    $"Nuomos terminas: {Color.Aqua.RGB()}{house.RentDuration}d.\n" +
                    $"{Color.White.RGB()}Nuomos kaina: {Color.Aqua.RGB()}${house.RentPrice}\n" +
                    $"{Color.White.RGB()}Interjeras: {Color.Aqua.RGB()}{House.GetInteriorDescription(house.InteriorIndex)}\n" +
                    $"{Color.White.RGB()}Jeigu jums viskas tinką  spauskite {Color.Green.RGB()}'Nuomotis'",
            "Nuomotis", "Išeiti");

            dialog.Response += (sender2, e2) =>
            {
                if (e2.DialogButton != DialogButton.Left) return;

                if (house.HasTenant())
                {
                    player.SendErrorMessage("Šį namą jau kažkas išsinuomavo!");
                    return;
                }
                if (house.RentPrice > player.GetMoney())
                {
                    player.SendErrorMessage("Jūs neturite pakankamai pinigų nuomai.");
                    return;
                }

                player.GiveMoney(-house.RentPrice);
                house.TenantName = player.Name;
                house.RentedUntil = Util.Date.GetUnix() + (86400 * house.RentDuration);
                house.UpdateWorldObjects();
                house.Save();
                player.SendSuccessMessage("Namas sėkmingai išnomuotas.");
            };

            dialog.Show(player);
        }
    }
}
