﻿using SampSharp.GameMode;
using SampSharp.GameMode.SAMP;
using SampSharp.GameMode.World;
using System;
using System.Collections.Generic;
using System.Linq;
using eastLIFEnet.Display;
using ExtensionMethods;
using SampSharp.GameMode.Definitions;
using SampSharp.GameMode.Display;

namespace eastLIFEnet
{
    public class House
    {
        public static readonly int MAX_HOUSES_PER_PLAYER = 2;
        public static readonly List<House> Houses = new List<House>();
        private static readonly List<Tuple<int, Vector3, string>> Interiors = new List<Tuple<int, Vector3, string>>
        {
            new Tuple<int, Vector3, string>(3, new Vector3(235.508994, 1189.169897, 1080.339966), "Didelis, 2 aukštų, 3 kambariai"),
            new Tuple<int, Vector3, string>(2, new Vector3(225.756989, 1240.000, 1082.149902), "Vidutinis, 1 aukšto, 1 kambarys"),
            new Tuple<int, Vector3, string>(1, new Vector3(223.043991, 1289.259888, 1082.199951), "Mažas, 1 aukšto, 1 kambarys"),
            new Tuple<int, Vector3, string>(7, new Vector3(225.630997, 1022.479980, 1084.069946), "Labai didelis, 2 aukštų, 4 kambariai"),
            new Tuple<int, Vector3, string>(15, new Vector3(295.138977, 1474.469971, 1080.519897), "Mažas, 1 aukšto, 2 kambariai"),
            new Tuple<int, Vector3, string>(15, new Vector3(328.493988, 1480.589966, 1084.449951), "Mažas, 1 aukšto, 2 kambariai #2"),
            new Tuple<int, Vector3, string>(15, new Vector3(385.803986, 1471.769897, 1080.209961), "Mažas, 1 aukšto, 1 kambarys, be tuoleto")
        };

        private TextLabel _label;
        private Pickup _pickup;

        public bool Lock { get; set; }
        public string OwnerName { get; set; }
        public int Price { get; }
        private int Sqlid { get; }
        public string TenantName { get; set; }
        public bool PlacedForRent { get; set; }
        public Vector3 Position { get; }
        public int InteriorIndex { get; }
        public int OwnerSafe { get; set; }
        public int TenantSafe { get; set; }
        public int RentedUntil { get; set; }
        public int RentPrice { get; set; }
        public int RentDuration { get; set; }

        private House(Vector3 position, int interiorIndex, int price, int sqlid)
        {
            Position = position;
            InteriorIndex = interiorIndex;
            Price = price;
            Sqlid = sqlid;
        }

        public static bool CMD_namas(Player player, string ps)
        {
            var house = FindNearestHouse(player);
            if (house == null || house.Position.DistanceTo(player.Position) > 2.5f)
            {
                player.SendErrorMessage("Jūs neesate prie namo!");
                return true;
            }
            if (!house.HasOwner())
            {
                HouseUI.BuyDialog(player, house);
                return true;
            }

            if (player.Name.Equals(house.OwnerName))
            {
                HouseUI.ControlDialog(player, house);
            }
            else if (player.Name.Equals(house.TenantName))
            {
                // TODO: atnaujinti nuoma
                player.SendInfoMessage("TODO");
            }
            else if (house.PlacedForRent)
            {
                HouseUI.RentDialog(player, house);
            }
            else
            {
                player.SendErrorMessage("Jūs neesate šio namo savininkas ar nuomininkas!");
            }

            return true;
        }

        public void Clear(bool clearOwner=false)
        {
            if(clearOwner)
                OwnerName = "nera";

            OwnerSafe = 0;
            PlacedForRent = false;
            Lock = false;
            RentPrice = 0;
            RentDuration = 0;
            Save();
        }

        public static List<House> GetOwnedHouses(Player player)
        {
            return new List<House>(Houses.Where(h => h.OwnerName.Equals(player.Name)));
        }

        public void UpdateWorldObjects()
        {
            _label?.Dispose();

            _pickup?.Dispose();

            if (HasOwner())
            {
                if(HasTenant())
                {
                    _label = new TextLabel($"Namas {Color.Orange.RGB()}#{Sqlid}\n\n"
                               + $"{Color.White.RGB()}Savininkas: {Color.SkyBlue.RGB()}{OwnerName}\n"
                               + $"{Color.White.RGB()}Nuomininkas: {Color.Aqua.RGB()}{TenantName}\n"
                               + $"{Color.White.RGB()}Išnomuotas iki: {Color.Aqua.RGB()}{Util.Date.FromUnix(RentedUntil):yyyy-MM-dd HH:mm:ss}\n"
                               + $"{Color.White.RGB()}Norėdami valdymas - {Color.SkyBlue.RGB()}/namas", Color.White, Position, 7.5f, 0, true);
                }
                else
                {
                    if(PlacedForRent)
                    {
                        _label = new TextLabel($"Namas {Color.Orange.RGB()}#{Sqlid}\n\n"
                                   + $"{Color.White.RGB()}Savininkas: {Color.SkyBlue.RGB()}{OwnerName}\n"
                                   + $"{Color.Silver.RGB()}Šis namas yra nuomojamas!\n"
                                   + $"{Color.White.RGB()}Nuomos terminas: {Color.Aqua.RGB()}{RentDuration}d.\n"
                                   + $"{Color.White.RGB()}Nuomos kaina: {Color.Aqua.RGB()}${RentPrice}\n"
                                   + $"{Color.White.RGB()}Norėdami išsinuomoti rašykite {Color.SkyBlue.RGB()}/namas", Color.White, Position, 7.5f, 0, true);
                    }
                    else
                    {
                        _label = new TextLabel($"Namas {Color.Orange.RGB()}#{Sqlid}\n\n"
                                      + $"{Color.White.RGB()}Savininkas: {Color.SkyBlue.RGB()}{OwnerName}\n"
                                      + $"{Color.White.RGB()}Namo valdymas - {Color.SkyBlue.RGB()}/namas", Color.White, Position, 7.5f, 0, true);
                    }
                }
                
                _pickup = Pickup.Create(1273, 1, Position);
            }
            else
            {
                _label = new TextLabel($"Namas {Color.Orange.RGB()}#{Sqlid}\n\n"
                                      + $"{Color.White.RGB()}Kaina: {Color.SkyBlue.RGB()}${Price}\n"
                                      + $"{Color.Orange.RGB()}Namas yra parduodamas!\n"
                                      + $"{Color.White.RGB()}Norėdami pirkti rašykite - {Color.Orange.RGB()}/namas", Color.White, Position, 7.5f, 0, true);
                _pickup = Pickup.Create(1272, 1, Position);
            }
        }

        public bool HasOwner()
        {
            return !OwnerName.Equals("nera");
        }

        public bool HasTenant()
        {
            return !TenantName.Equals("nera");
        }

        public Player GetOwner()
        {
            return Player.Get(OwnerName);
        }

        public Player GetTenant()
        {
            return Player.Get(TenantName);
        }

        public void CheckRent()
        {
            if (!HasTenant()) return;
            if (Util.Date.GetUnix() <= RentedUntil) return;

            TenantName = "nera";
            UpdateWorldObjects();
            Save();
        }

        public void Save()
        {
            var query = new DBQuery($"UPDATE `houses` SET owner='{OwnerName}', tenant='{TenantName}', locked='{Convert.ToInt32(Lock)}', placedForRent='{Convert.ToInt32(PlacedForRent)}', ownerSafe='{OwnerSafe}', tenantSafe='{TenantSafe}', rentedUntil='{RentedUntil}' WHERE id='{Sqlid}'", QueryType.INSERT_OR_DELETE);
            query.Execute();
            query = new DBQuery($"UPDATE `houses` SET rentPrice='{RentPrice}', rentDuration='{RentDuration}' WHERE id='{Sqlid}'", QueryType.INSERT_OR_DELETE);
            query.Execute();
        }

        public static House FindNearestHouse(Player player)
        {
            House nearestHouse = null;
            foreach (var house in Houses)
            {
                if (nearestHouse == null)
                {
                    nearestHouse = house;
                    continue;
                }

                if (house.Position.DistanceTo(player.Position) < nearestHouse.Position.DistanceTo(player.Position))
                    nearestHouse = house;
            }
            return nearestHouse;
        }

        public static string GetInteriorDescription(int index)
        {
            return Interiors[index].Item3;
        }

        public static void LoadAll()
        {
            Console.WriteLine("> Loading houses...");
            Console.WriteLine($"> Found {DBQuery.GetNumberRowsInTable("houses")} houses...");

            var query = new DBQuery("SELECT * FROM `houses`");
            query.Execute();

            if(query.GetResult().HasRows)
            {
                while(query.GetResult().Read())
                {
                    var position = new Vector3(query.GetResult().GetFloat("posX"), query.GetResult().GetFloat("posY"), query.GetResult().GetFloat("posZ"));

                    var house = new House(position, query.GetResult().GetInt32("interiorIndex"), query.GetResult().GetInt32("price"), query.GetResult().GetInt32("id"))
                    {
                        Lock = query.GetResult().GetBoolean("locked"),
                        OwnerName = query.GetResult().GetString("owner"),
                        TenantName = query.GetResult().GetString("tenant"),
                        PlacedForRent = query.GetResult().GetBoolean("placedForRent"),
                        OwnerSafe = query.GetResult().GetInt32("ownerSafe"),
                        TenantSafe = query.GetResult().GetInt32("tenantSafe"),
                        RentedUntil = query.GetResult().GetInt32("rentedUntil"),
                        RentPrice = query.GetResult().GetInt32("rentPrice"),
                        RentDuration = query.GetResult().GetInt32("rentDuration")
                    };

                    house.UpdateWorldObjects();

                    Houses.Add(house);
                }
            }

            query.Close();
        }
    }
}
