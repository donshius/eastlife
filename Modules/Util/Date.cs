﻿using System;

namespace eastLIFEnet.Util
{
    public class Date
    {
        public static int GetUnix()
        {
            return (int)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
        }

        public static DateTime FromUnix(int unix)
        {
            var dt = new DateTime(1970, 1, 1, 0, 0, 0, 0).AddSeconds(unix);
            return dt;
        }
    }
}