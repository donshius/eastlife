﻿using SampSharp.GameMode.SAMP.Commands.PermissionCheckers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SampSharp.GameMode.World;

namespace eastLIFEnet.Util.Permissions
{
    class AdminPermission : IPermissionChecker
    {
        public string Message
        {
            get
            {
                return string.Format("{{{0}}}[ {{{1}}}KLAIDA {{{2}}}] {{{3}}}:: {{{4}}}Jūs neturite teisės naudotis šia komanda!", Util.Color.HEX_WHITE, Util.Color.HEX_RED, Util.Color.HEX_WHITE, Util.Color.HEX_SILVER, Util.Color.HEX_WHITE);
            }
        }

        public bool Check(BasePlayer player)
        {
            return true;
        }
    }
}
