﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eastLIFEnet.Util
{
    class Color
    {
        public static string HEX_WHITE = "ffffff";
        public static string HEX_SILVER = "c0c0c0";
        public static string HEX_GRAY = "808080";
        public static string HEX_BLACK = "000000";
        public static string HEX_RED = "ff0000";
        public static string HEX_MAROON = "800000";
        public static string HEX_YELLOW = "ffff00";
        public static string HEX_OLIVE = "808000";
        public static string HEX_LIME = "00ff00";
        public static string HEX_GREEN = "008000";
        public static string HEX_AQUA = "00ffff";
        public static string HEX_TEAL = "008080";
        public static string HEX_BLUE = "0000ff";
        public static string HEX_NAVY = "000080";
        public static string HEX_FUCHSIA = "ff00ff";
        public static string HEX_PURPLE = "800080";
        public static string HEX_ORANGE = "ffa500";
        public static string HEX_DEEPSKYBLUE = "00bfff";
        public static string HEX_DARKGREEN = "006400";
        public static string HEX_LIGHTGREEN = "00ee00";
        public static string HEX_DEEPSKYBLUEDARK = "00688b";
    }
}
