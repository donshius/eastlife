﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using eastLIFEnet;
using eastLIFEnet.World;

namespace ExtensionMethods
{
    public static class Extensions
    {
        /* http://www.netgfx.com/RGBaZR/ */
        public static string RGB(this SampSharp.GameMode.SAMP.Color color)
        {
            return color.ToString(SampSharp.GameMode.SAMP.ColorFormat.RGB);
        }

        public static bool sscanf(this string formatString, string format, out object[] formatParams)
        {
            formatParams = new object[format.Length];
            if (format.Length == 0) return false;
            if (formatString.Length == 0) return false;
            var parameters = formatString.Split(' ');

            if (parameters.Length != format.Length)
                return false;

            for (var i = 0; i < format.Length; i++)
            {
                if (format[i] == 's')
                {
                    formatParams[i] = (string) parameters[i];
                }
                else if (format[i] == 'i')
                {
                    if (!int.TryParse(parameters[i], out int intRes))
                        return false;
                    formatParams[i] = intRes;
                }
                else if (format[i] == 'f')
                {
                    if (!float.TryParse(parameters[i], out float floatRes))
                        return false;
                    formatParams[i] = floatRes;
                }
                else if (format[i] == 'u')
                {
                    formatParams[i] = Player.Get(parameters[i]);
                }
            }
            return true;
        }
    }
}
