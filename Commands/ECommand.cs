﻿using SampSharp.GameMode.SAMP.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SampSharp.GameMode.SAMP.Commands.PermissionCheckers;
using System.Reflection;
using SampSharp.GameMode.World;
using eastLIFEnet.World;
using SampSharp.GameMode.SAMP;
using ExtensionMethods;

namespace eastLIFEnet.Commands
{
    class ECommand : DefaultCommand
    {
        public ECommand(CommandPath[] names, string displayName, bool ignoreCase, IPermissionChecker[] permissionCheckers, MethodInfo method, string usageMessage) : base(names, displayName, ignoreCase, permissionCheckers, method, usageMessage)
        {
        }

        protected override bool SendUsageMessage(BasePlayer player)
        {
            if(UsageMessage == null)
            {
                //((Player)player).SendCustomMessage("KOMANDA", Color.Orange.RGB(), $"Naudojimas: {Color.Silver.RGB()}{this}");
                return true;
            }

            return true;
        }
    }
}
