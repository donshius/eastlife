﻿using System;
using eastLIFEnet.World;

namespace eastLIFEnet.Commands
{
    public class CustomCommand
    {
        public static bool Process(Player player, string command)
        {


            var commandMethod = typeof(Player).GetMethod("test");
            if (commandMethod == null)
                return false;

            return (bool)commandMethod.Invoke(player, new[] {""});
        }
    }
}